const options = {
  duration: 1500,
  iterations: 1,
  fill: "forwards",
  easing: "ease-in-out",
};

export function mainContentAnimation(ref, scrollData) {
  const { isDown, index } = scrollData;

  const keyFrame = [
    { transform: "translateY(0)" },
    { transform: "translateY(-100%)" },
  ];

  if (isDown && index === 1) {
    ref.current.animate(keyFrame, options).play();
  } else if (isDown === false && index === 0) {
    ref.current.animate(keyFrame, { ...options, fill: "backwards" }).reverse();
  } else if (isDown === null && index === 0)
    ref.current
      .animate(keyFrame, { ...options, fill: "backwards", iterations: 0 })
      .reverse();
}

export function imgCircleAnimation(ref, scrollData) {
  const { isDown, index } = scrollData;

  const scaleKeyFrame = [
    { transform: "scaleY(1)" },
    { transform: "scale(10)" },
  ];

  const changeBgColorKeyFrame = [
    {
      background: "#eff4fe",
    },
    { background: "rgba(249, 245, 238, 1)" },
  ];

  if (isDown && index === 1) ref.current.animate(scaleKeyFrame, options).play();
  else if (isDown === false && index === 0) {
    ref.current
      .animate(scaleKeyFrame, { ...options, fill: "backwards" })
      .reverse();
  } else if (isDown && index === 2)
    ref.current.animate(changeBgColorKeyFrame, options).play();
  else if (isDown === false && index === 1)
    ref.current
      .animate(changeBgColorKeyFrame, { ...options, fill: "backwards" })
      .reverse();
  else if (isDown === null && index === 0) {
    ref.current
      .animate(changeBgColorKeyFrame, {
        ...options,
        fill: "backwards",
        iterations: 0,
      })
      .reverse();
    ref.current
      .animate(scaleKeyFrame, { ...options, fill: "backwards", iterations: 0 })
      .reverse();
  }
}

export function imgAnimation(ref, scrollData) {
  const { isDown, index } = scrollData;

  const positionKeyFrame = [
    { bottom: "-10%", left: 0, height: "55vh" },
    { left: "50%", bottom: "4%", height: "70%" },
  ];

  const changeBgImgKeyFrame = [
    {
      background: 'url("img/kid-1.png") no-repeat',
      height: "70%",
      width: "25vw",
      backgroundPosition: "center",
      backgroundSize: "contain",
    },
    {
      background: 'url("img/kid-2.png") no-repeat',
      height: "70%",
      width: "25vw",
      backgroundPosition: "center",
      backgroundSize: "contain",
    },
  ];

  if (isDown && index === 1) {
    ref.current.animate(positionKeyFrame, options).play();
  } else if (isDown === false && index === 0) {
    ref.current
      .animate(positionKeyFrame, { ...options, fill: "backwards" })
      .reverse();
  } else if (isDown && index === 2)
    ref.current.animate(changeBgImgKeyFrame, options).play();
  else if (isDown === false && index !== 2)
    ref.current
      .animate(changeBgImgKeyFrame, { ...options, fill: "backwards" })
      .reverse();
  else if (isDown === null && index === 0) {
    ref.current
      .animate(changeBgImgKeyFrame, {
        ...options,
        fill: "backwards",
        iterations: 0,
      })
      .reverse();
    ref.current
      .animate(positionKeyFrame, {
        ...options,
        fill: "backwards",
        iterations: 0,
      })
      .reverse();
  }
}

export function smallCircleAnimation(ref, scrollData) {
  const { isDown, index } = scrollData;

  const positionKeyFrame = [{ left: "-4%" }, { left: "75%" }];

  const changeShapeColorKeyFrame = [
    {
      left: "75%",
      backgroundImage: "linear-gradient(to right,#202da5,#2032d4)",
    },
    {
      left: "70%",
      backgroundImage: "linear-gradient(to right,#FA6D30,#E4067E)",
    },
  ];

  if (isDown && index === 1) {
    ref.current.animate(positionKeyFrame, options).play();
  } else if (isDown === false && index === 0)
    ref.current
      .animate(positionKeyFrame, { ...options, fill: "backwards" })
      .reverse();
  else if (isDown && index === 2) {
    ref.current.animate(changeShapeColorKeyFrame, options).play();
  } else if (isDown === false && index === 1)
    ref.current
      .animate(changeShapeColorKeyFrame, { ...options, fill: "backwards" })
      .reverse();
  else if (isDown === null && index === 0) {
    ref.current
      .animate(changeShapeColorKeyFrame, {
        ...options,
        fill: "backwards",
        iterations: 0,
      })
      .reverse();
    ref.current
      .animate(positionKeyFrame, {
        ...options,
        fill: "backwards",
        iterations: 0,
      })
      .reverse();
  }
}

export function bigCircleAnimation(ref, scrollData) {
  const { isDown, index } = scrollData;

  const positionKeyFrame = [
    {
      transform: "scaleY(2) translateX(0px)",
    },
    {
      transform: "translateX(100%)",
    },
  ];

  if (isDown && index === 1) {
    ref.current.animate(positionKeyFrame, options).play();
  } else if (isDown === false && index === 0) {
    ref.current
      .animate(positionKeyFrame, { ...options, fill: "backwards" })
      .reverse();
  } else if (isDown === null && index === 0)
    ref.current
      .animate(positionKeyFrame, {
        ...options,
        fill: "backwards",
        iterations: 0,
      })
      .reverse();
}
