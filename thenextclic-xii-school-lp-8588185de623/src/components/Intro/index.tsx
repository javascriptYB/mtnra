import React, { useEffect, useRef, useState } from "react";
import { LogoIcon } from "../../../assets/icons";
import useDevice from "../../hooks/useDevice";
import useScroll from "../../hooks/useScroll";
import sectionScroll from "../../utils";
import {
  bigCircleAnimation,
  imgAnimation,
  imgCircleAnimation,
  mainContentAnimation,
  smallCircleAnimation,
} from "./animation";

import {
  BigCircle,
  Container,
  ImgCircle,
  ImgContainer,
  ImgWrapper,
  LogoContainer,
  MainContent,
  SmallCircle,
} from "./styles";

interface IntroProps {
  data: {
    description: string;
    subtitle: string;
    title: string;
    title_2: string;
  };
}

const Intro = (props: IntroProps) => {
  const scrollData = useScroll();
  const device = useDevice();
  const ref = useRef();
  const mainContentRef = useRef(null);
  const imgCircleRef = useRef(null);
  const imgAnimationRef = useRef(null);
  const smallCircleRef = useRef(null);
  const bigCircleRef = useRef(null);

  const { data } = props;

  useEffect(() => {
    if (device.isDesktop) {
      if (scrollData.index === 0 && !scrollData.isDown) {
        sectionScroll(ref.current, 800);
      }
      mainContentAnimation(mainContentRef, scrollData);
      imgCircleAnimation(imgCircleRef, scrollData);
      imgAnimation(imgAnimationRef, scrollData);
      smallCircleAnimation(smallCircleRef, scrollData);
      bigCircleAnimation(bigCircleRef, scrollData);
    }
  }, [scrollData, device]);

  useEffect(() => {
    if (!device.isDesktop) sectionScroll(ref.current, 800);
  }, [device]);

  return (
    <Container ref={ref}>
      <MainContent ref={mainContentRef}>
        <LogoContainer>
          <LogoIcon
            style={{
              backgroundColor: "white",
              padding: "2px",
              borderRadius: "100%",
              width: "60px",
              height: "60px",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              marginBottom: "1vh",
            }}
          />
        </LogoContainer>
        <h1>
          {data?.title} <span>{data?.title_2}</span>
        </h1>
        <p>{data?.subtitle}</p>
        <p>{data?.description}</p>
      </MainContent>
      <ImgWrapper>
        <ImgCircle ref={imgCircleRef} />
        <ImgContainer ref={imgAnimationRef} />
      </ImgWrapper>

      <SmallCircle ref={smallCircleRef} />

      <BigCircle ref={bigCircleRef} />
    </Container>
  );
};

export default Intro;
