import styled, { keyframes, css } from "styled-components";

export const Container = styled.section`
  height: 100vh;
  position: relative;
  overflow: hidden;

  @media only screen and (max-width: 820px) {
    height: 100%;
  }
`;

export const ImgWrapper = styled.div`
  @media only screen and (max-width: 820px) {
    height: 400px;
    width: 100%;
    position: relative;
    overflow: hidden;
    animation: unset;
  }

  @media only screen and (min-width: 700px) and (max-width: 820px) {
    height: 550px;
    animation: unset;
  }
`;

const imgCircleAnimation = keyframes`
  100% {
    -webkit-transform: scale(10);
    -moz-transform: scale(10);
    -o-transform: scale(10);
    -ms-transform: scale(10);
    transform: scale(10);
  }
`;

const imgCirlceChangeColor = keyframes`
  100% {
    background-color: rgba(249, 245, 238, 1);
    // background-color: yellow;
  }
`;

const imgCircleAnimationReverse = keyframes`
100% {
  -webkit-transform: scale(10);
  -moz-transform: scale(10);
  -o-transform: scale(10);
  -ms-transform: scale(10);
  transform: scale(10);
}
`;

export const ImgCircle = styled.div`
  border-radius: 100%;
  height: 25vw;
  width: 25vw;
  max-height: 650px;
  max-width: 650px;
  min-height: 400px;
  min-width: 400px;
  background-color: #eff4fe;
  // background-color: red;
  position: absolute;
  bottom: -25%;
  left: 0;
  right: 0;
  margin-left: auto;
  margin-right: auto;
  transform: scaleY(1);

  @media only screen and (max-width: 820px) {
    max-height: unset;
    max-width: unset;
    min-height: unset;
    min-width: unset;
    height: 80vw;
    width: 80vw;
    max-height: 450px;
    max-width: 450px;
    min-height: 250px;
    min-width: 250px;
    bottom: -10%;
    animation: unset;
  }
`;

// export const ImgContainer = styled.div`
//   max-width: 30vw;
//   position: absolute;
//   height: 30vw;
//   align-self: center;
//   bottom: -15%;
//   left: 0;
//   right: 0;
//   margin-left: auto;
//   margin-right: auto;
//   background-color: red;
//   // background-position: center;
//   // background-size: contain;
//   // animation: ${imgContainerAnimation} 2s ease-in-out forwards;
// `;

const imgContainerAnimation = keyframes`
  100% {
    left: 50%;
    bottom: 4%;
    height: 70%;
  }
`;

const imgContainerAnimationReverse = keyframes`
  0%{

  }
  100% {
    left: 50%;
    bottom: 4%;
    height: 70%;
  }
`;

const imgChangeBg = keyframes`
  100% {
    background: url("img/kid-2.png") no-repeat;
    height: 70%;
    width: 25vw;
    background-position: center;
    background-size: contain;
  }
`;

const imgChangeBgReverse = keyframes`
  0%{

  }
  100% {
    background: url("img/kid-1.png") no-repeat;
    height: 70%;
    width: 25vw;
    background-position: center;
    background-size: contain;
  }
`;

export const ImgContainer = styled.div`
  position: absolute;
  bottom: -10%;
  left: 0;
  right: 0;
  margin-left: auto;
  margin-right: auto;
  background: url(img/kid-1.png) no-repeat;
  height: 55vh;
  width: 25vw;
  background-position: center;
  background-size: contain;
  max-height: 700px;
  max-width: 700px;

  @media only screen and (max-width: 820px) {
    height: 100%;
    width: 80%;
    bottom: 0%;
    max-height: unset;
    max-width: unset;
    min-height: unset;
    min-width: unset;
    position: absolute;
    z-index: 9999;
    animation: unset;
  }
`;

const smallCricleAnimation = keyframes`
  100% {
    left: 75%;
  }
`;

const smallCricleAnimationReverse = keyframes`
  0%{
    
  }
  100% {
    left: 75%;
  }
`;

const smallCircleChangeColor = keyframes`
0% {
  background-image: linear-gradient(
    to right,
    rgba(250, 109, 48, 1),
    rgba(228, 6, 126, 1)
  );
}
100% {
  left: 70%;
  background-image: linear-gradient(
    to right,
    rgba(250, 109, 48, 1),
    rgba(228, 6, 126, 1)
  );
}
`;

const smallCircleChangeColorReverse = keyframes`
  0% {
    background-image: linear-gradient(to right, #202da5, #2032d4);
  }
  100% {
    left: 70%;
    background-image: linear-gradient(to right, #202da5, #2032d4);
  }
`;

export const SmallCircle = styled.div`
  height: 8vw;
  width: 8vw;
  border-radius: 100%;
  transform: scale(2);
  position: absolute;
  background-image: linear-gradient(to right, #202da5, #2032d4);
  left: -4%;
  top: -5%;

  @media only screen and (max-width: 820px) {
    height: 12vh;
    width: 20vw;
    animation: unset;

    // for tabelt
    @media only screen and (min-width: 700px) and (max-width: 820px) {
      height: 12vh;
      width: 20vw;
      left: -10%;
      top: -12%;
    }
  }
`;

/* big circle */

const bigCricleAnimation = keyframes`
  0% {
    right: -22%;
  }

  100% {
    right: -100%;
  }
`;

const bigCricleAnimationReverse = keyframes`
  0% {
    right: -22%;
  }
  50% {
    
  }
  100% {
    right: -100%;
  }
`;

export const BigCircle = styled.div`
  height: 40%;
  width: 30%;
  background-image: linear-gradient(to right, #fa6d30, #e4067e);
  position: absolute;
  border-radius: 100%;
  right: -22%;
  margin: auto;
  top: 0;
  bottom: 0;
  transform: scaleY(2);
  @media only screen and (max-width: 820px) {
    height: 20vh;
    width: 30vw;
    animation: unset;
  }
`;

const mainContentAnimation = keyframes`
  100% {
    transform: translateY(-100vh);
  }
`;

const mainContentAnimationReverse = keyframes`
  %0{
    
  }
  100% {
    top: -100vh;
  }
`;

export const MainContent = styled.div`
  padding: 20px;
  max-width: 800px;
  position: absolute;
  left: 0;
  right: 0;
  margin: auto;
  top: 0;
  top: 5%;
  transform: translateY(0);

  text-align: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  @media only screen and (max-width: 820px) {
    justify-content: flex-start;
    align-items: flex-start;
    position: relative;
    margin-top: 30px;
  }

  @media only screen and (max-height: 800px) {
    top: 2%;
    max-width: 650px;
  }

  & > p:first-of-type {
    color: #060a2b;
    font-size: 1.1rem;
    padding: 10px;
    font-weight: 500;
    font-size: 1.125rem;
    // @media only screen and (max-height: 800px) {
    //   font-size: 0.9rem;
    // }
  }

  & > p:last-of-type {
    color: #060a2b;
    font-size: 1rem;
    line-height: 22px;
    font-weight: 400;
    max-width: 90%;
    padding: 10px;
    // @media only screen and (max-height: 800px) {
    //   font-size: 0.8rem;
    //   padding: 2px 0px;
    // }
  }

  & > h1 {
    font-size: 3.5rem;
    line-height: 65px;
    padding: 20px 0px;
    max-width: 90%;
    @media only screen and (max-height: 800px) {
      font-size: 2.5rem;
      line-height: 40px;
    }
  }

  & > h1 > span {
    color: #bf2172;
  }

  @media only screen and (max-width: 820px) {
    animation: unset;

    & > h1 {
      font-size: 1.3rem;
      text-align: start;
      line-height: 28px;
    }

    & > p:first-of-type {
      padding: 0;
      margin-bottom: 10px;
      text-align: start;
      font-size: 1rem;
    }

    & > p:last-of-type {
      font-size: 1rem;
      line-height: 22px;
      font-weight: 400;
      text-align: start;
      padding: 0;
    }

    // for tabelt
    @media only screen and (min-width: 700px) and (max-width: 820px) {
      & > h1 {
        font-size: 2rem;
        line-height: 40px;
        max-width: 500px;
      }

      & > p:first-of-type {
        font-size: 1.1rem;
        max-width: 500px;
      }

      & > p:last-of-type {
        font-size: 1.1rem;
        max-width: 500px;
      }
    }
  }
`;

export const LogoContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  @media only screen and (max-width: 820px) {
    align-self: center;
  }
`;
