import styled, { keyframes, css } from "styled-components";

export const FooterContainer = styled.footer`
  display: flex;
  align-self: flex-end;
  background-color: #f6f9ff;
  max-height: 180px;
  min-height: 150px;
  height: 22%;
  width: 100%;
  position: relative;
  justify-content: center;
  align-items: center;
  flex-direction: column;

  @media only screen and (max-width: 820px) {
    height: 350px;
    max-height: unset;
  }
`;

export const FooterRow = styled.div`
  flex-direction: row;
  width: 85%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-bottom: 0.5px solid #e2e2e2;
  padding-top: 5px;
  flex: 1;

  @media only screen and (max-width: 820px) {
    flex-direction: column;
    border-bottom: none;
    border-bottom: 0.5px solid #e2e2e2;
  }
`;

export const FlexContainer = styled.div`
  flex: 2;
  display: flex;
  justify-content: space-between;
  @media only screen and (max-width: 820px) {
    flex-direction: column-reverse;
    align-items: center;
    flex: unset;
  }
`;

export const ConditionRow = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 85%;
  align-items: center;
  flex: 1;
  padding: 8px 0;

  & > div {
    flex: 1;
    @media only screen and (max-width: 820px) {
      flex: unset;
    }
  }

  & > h3:first-of-type {
    display: flex;
    align-items: center;
    justify-content: center;
  }

  & > h3 {
    flex: 1;
    color: #707070;
    font-size: 0.8rem;
  }

  & > h3:last-of-type {
    text-align: end;

    & > a {
      cursor: pointer;
      color: #707070;
      font-size: 0.8rem;
      text-decoration: none;
    }
  }
`;

export const KeepConnected = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;

  & > h6 {
    color: #393939;
    font-size: 0.9rem;
    font-weight: 500;
  }

  @media only screen and (max-width: 820px) {
    & > h6 {
      font-size: 0.9rem;
    }

    // for tablet
    @media only screen and (min-width: 700px) and (max-width: 820px) {
      & > h6 {
        font-size: 1rem;
      }
    }
  }
`;

export const SocialMediaIcons = styled.div`
  display: flex;
  padding: 10px 0px;
`;

export const SocialMediafacebook = styled.div`
  margin: 1px 10px;
`;

export const StoreImgContainer = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: flex-end;

  & > h4 {
    font-size: 1rem;
    color: #191919;
    font-weight: 700;
  }

  @media only screen and (max-width: 820px) {
    align-items: center;
    & > h4 {
      font-size: 1rem;
    }

    // for tablet
    @media only screen and (min-width: 700px) and (max-width: 820px) {
      & > h4 {
        font-size: 1rem;
      }
    }
  }
`;

export const StoreImgRow = styled.div`
  display: flex;
  flex-direction: row;
  padding: 10px 0px;

  & > img:first-of-type {
    margin-right: 10px;
  }
`;

export const LogoText = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;

  & > h3 {
    padding: 16px 0;
    font-size: 1rem;
    color: #191919;
    font-weight: 700;
  }

  @media only screen and (max-width: 820px) {
    & > h3 {
      padding: 8px 0;
      font-size: 1rem;
      color: #191919;
      font-weight: 700;
    }

    // for tablet
    @media only screen and (min-width: 700px) and (max-width: 820px) {
      & > h3 {
        font-size: 1.1rem;
      }
    }
  }
`;

export const FooterLogo = styled.div`
  background-color: white;
  padding: 0;
  margin: 0;
  border-radius: 100%;
`;

export const FooterLeftCircle = styled.div`
  position: absolute;
  height: 89%;
  width: 8%;
  border-radius: 100%;
  background-color: #ffbb44;
  left: -6.5%;
  top: 0;
  bottom: 0;
  margin: auto;
  transform: scale(1.1);

  @media only screen and (max-width: 820px) {
    position: absolute;
    height: 20vw;
    width: 20vw;
    border-radius: 100%;
    background-color: #ffbb44;
    left: -15.5%;
    bottom: unset;
    top: 10%;
    margin: auto;
    transform: scale(1.1);
  }
`;

export const FooterRightCircle = styled.div`
  position: absolute;
  height: 5vw;
  width: 6vw;
  right: -5%;
  background-image: linear-gradient(#d5189d, #e800ba);
  border-radius: 100%;
  top: 4%;
  transform: scale(1.1);

  @media only screen and (max-width: 820px) {
    position: absolute;
    height: 10vw;
    width: 10vw;
    right: -10%;
    background-image: linear-gradient(#d5189d, #e800ba);
    border-radius: 100%;
    top: unset;
    bottom: 30%;
    transform: scale(2);
  }
`;
