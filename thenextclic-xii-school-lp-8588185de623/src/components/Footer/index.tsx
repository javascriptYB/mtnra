import Link from "next/link";
import { InstagramIcon, LogoIcon } from "../../../assets/icons";
import Facebook from "../../../assets/icons/Facebook";
import {
  ConditionRow,
  FlexContainer,
  FooterContainer,
  FooterLeftCircle,
  FooterRightCircle,
  FooterRow,
  KeepConnected,
  LogoText,
  SocialMediaIcons,
  StoreImgContainer,
  StoreImgRow,
} from "./styles";

const Footer = () => {
  return (
    <FooterContainer>
      <FooterRow>
        <FlexContainer>
          <KeepConnected>
            <h6>Restez connectés</h6>

            <SocialMediaIcons>
              <Facebook
                style={{
                  margin: "1px 10px",
                }}
              />
              <InstagramIcon />
            </SocialMediaIcons>
          </KeepConnected>

          <LogoText>
            <LogoIcon
              style={{
                backgroundColor: "white",
                padding: "2px",
                borderRadius: "100%",
                width: "60px",
                height: "60px",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                marginBottom: "1vh",
              }}
            />
            <h3>Student Empowerment</h3>
          </LogoText>
        </FlexContainer>

        <StoreImgContainer>
          <h4>Télécharger l'application</h4>
          <StoreImgRow>
            <img src="img/google-play.png" />
            <img src="img/apple-store.png" />
          </StoreImgRow>
        </StoreImgContainer>
      </FooterRow>

      <ConditionRow>
        <div></div>
        <h3>© XII School 2022. Tous droits réservés</h3>
        <h3>
          <Link href="/terms">Conditions générales</Link>
        </h3>
      </ConditionRow>

      <FooterLeftCircle />
      <FooterRightCircle />
    </FooterContainer>
  );
};

export default Footer;
