import { forwardRef, useEffect, useRef } from "react";
import useScroll from "../../hooks/useScroll";
import {
  LeftBtnCircle,
  RightBtnCircle,
  TeacherImg,
  TeacherInfo,
  TeacherInfoCard,
  TeacherInfoCardContainer,
  TeacherInfoImgRow,
  TeacherInfoQuote,
  TeacherPersonalInfo,
  TeachersContainer,
} from "./styles";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { LeftArrowIcon } from "../../../assets/icons";
import RightArrow from "../../../assets/icons/RightArrow";
import sectionScroll from "../../utils";
import useDevice from "../../hooks/useDevice";
import { cardAnimation, cardContainerAnimation } from "./animations";

interface TeacherCardCmpProps {
  duration: number;
  data: {
    description: string;
    image: { uri: string; alt: string };
    name: string;
    nid: string;
    specialite: string;
    titre: string;
    type: string;
    uri: string;
    uuid: string;
  };
}

const TeacherCardCmp = (props: TeacherCardCmpProps) => {
  const ref = useRef(null);
  const scrollData = useScroll();
  const device = useDevice();

  const { data, duration } = props;

  useEffect(() => {
    if (device?.isDesktop) cardAnimation(ref, scrollData, duration);
  }, [scrollData]);

  return (
    <TeacherInfoCard ref={ref} key={data?.uuid}>
      <TeacherInfoQuote src="img/quotes.png" />
      <TeacherInfoImgRow>
        <TeacherImg src={data?.image?.uri} atl={data?.image?.uri} />
        <TeacherPersonalInfo>
          <h4>{data?.name}</h4>
          <p>{data?.specialite}</p>
        </TeacherPersonalInfo>
      </TeacherInfoImgRow>
      <p>{data?.description}</p>
    </TeacherInfoCard>
  );
};

const NextArrow = (props) => {
  const { onClick, currentSlide, slideCount } = props;

  let disabled = currentSlide === slideCount - 1;

  return (
    <RightBtnCircle onClick={onClick} disabled={disabled}>
      <RightArrow disabled={disabled} />
    </RightBtnCircle>
  );
};

const PrevArrow = (props) => {
  const { onClick, currentSlide, slideCount } = props;

  let disabled = currentSlide === 0;

  return (
    <LeftBtnCircle onClick={onClick} disabled={disabled}>
      <LeftArrowIcon disabled={disabled} />
    </LeftBtnCircle>
  );
};

interface TeachersProps {
  prevSection: React.RefObject<HTMLInputElement>;
  nextSection: React.RefObject<HTMLInputElement>;
  data: {
    temoignages: [
      {
        description: string;
        image: { uri: string; alt: string };
        name: string;
        nid: string;
        specialite: string;
        titre: string;
        type: string;
        uri: string;
        uuid: string;
      }
    ];
    title: string;
  };
}

const Teachers = forwardRef((props: TeachersProps, ref: any) => {
  const scrollData = useScroll();
  const device = useDevice();
  const cardContainerRef = useRef(null);

  const { data } = props;

  useEffect(() => {
    if (device?.isDesktop) {
      if (scrollData.index === 5 && scrollData.isDown) {
        sectionScroll(props.nextSection.current, 1500);
      } else if (scrollData.index === 3 && scrollData.isDown === false) {
        sectionScroll(props.prevSection.current, 1500);
      }
      cardContainerAnimation(cardContainerRef, scrollData);
    }
  }, [scrollData]);

  const settings = {
    className: "slider variable-width",
    slidesToShow: 1,
    slidesToScroll: 1,
    variableWidth: true,
    infinite: false,
    rows: 1,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
  };

  return (
    <TeachersContainer ref={ref}>
      <TeacherInfo>
        <h1>{data?.title}</h1>
        <TeacherInfoCardContainer ref={cardContainerRef}>
          <Slider {...settings}>
            {data?.temoignages?.length > 0 &&
              data?.temoignages?.map((item, index) => (
                <TeacherCardCmp data={item} duration={index + 500} />
              ))}
          </Slider>
        </TeacherInfoCardContainer>
      </TeacherInfo>
    </TeachersContainer>
  );
});

export default Teachers;
