import styled, { keyframes, css } from "styled-components";

export const TeachersContainer = styled.section`
  height: 100vh;
  width: 100vw;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  // scroll-snap-align: center;
  overflow: hidden;
  background-color: #f9fcff;

  @media only screen and (max-width: 820px) {
    align-items: flex-start;
    height: 100%;
  }
`;

export const TeacherInfo = styled.div`
  display: flex;
  flex-direction: column;
  width: 80%;
  & > h1 {
    color: #000000;
    font-size: 2rem;
    margin: 50px 0px;
  }

  @media only screen and (max-width: 820px) {
    display: flex;
    flex-direction: column;
    width: 90%;
    & > h1 {
      color: #000000;
      font-size: 1.5rem;
      margin: 30px 0px;
      margin-bottom: 10px;
      max-width: 200px;
    }

    // for tablet
    @media only screen and (min-width: 700px) and (max-width: 820px) {
      & > h1 {
        max-width: 350px;
        font-size: 2rem;
      }
    }
  }
`;

const TeacherInfoCardContainerAnimation = keyframes`
  100%{
    left: 0;
  }
`;

const TeacherInfoCardContainerAnimationReverse = keyframes`
  0 % {

  }
  100% {
    left: 0;
  }
`;

export const TeacherInfoCardContainer = styled.div`
  display: block;
  margin: 12px;
  width: 100%;
  position: relative;

  @media only screen and (max-width: 820px) {
    height: 450px;
    animation: unset;
    display: block;
    margin: 12px;
    width: 100%;
    position: relative;
    left: -5%;
    /* background: red; */

    // for tablet
    @media only screen and (min-width: 700px) and (max-width: 820px) {
      height: 450px;
    }
  }
`;

const TeacherInfoCardAnimation = keyframes`
  100% {
    left: 0%;
  }
`;

export const TeacherInfoCard = styled.div`
  background-color: #ffffff;
  // display: block;
  // flex-direction: column;
  position: relative;
  left: 40%;
  width: 450px;
  padding: 24px;
  box-shadow: 0px 4px 16px #0000000d;
  border-radius: 10px;
  margin: 12px 12px;
  position: relative;
  height: 250px;
  overflow: hidden;

  & > p {
    color: #000000;
    font-size: 1rem;
    font-weight: 500;
  }

  @media only screen and (max-width: 820px) {
    margin: 12px;
    left: 0%;

    & > p {
      font-size: 0.8rem;
    }

    // for tablet
    @media only screen and (min-width: 700px) and (max-width: 820px) {
      max-height: 350px;
      margin: 12px;
      width: 450px;

      & > p {
        font-size: 1.1rem;
      }
    }
  }
`;

export const TeacherInfoImgRow = styled.div`
  display: flex;
  flex-direction: row;
  align-items: baseline;
  padding-bottom: 16px;
`;

export const TeacherInfoBtnContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  margin-right: 10%;
  margin-top: 50px;

  @media only screen and (max-width: 820px) {
    justify-content: flex-start;
    margin-top: 10px;
    padding: 10px 0;
  }
`;

export const RightBtnCircle = styled.div`
  display: flex;
  height: 50px;
  width: 50px;
  margin: 0px 16px;
  align-self: center;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${(props) => (props.disabled ? "#EFF4FE" : "#bf2172")};
  border-radius: 100%;
  position: absolute;
  right: 5%;
  bottom: -20%;
  cursor: pointer;

  @media only screen and (max-width: 820px) {
    bottom: -20%;
    right: unset;
    left: 0;
    margin-left: 80px;
    height: 40px;
    width: 40px;

    // for tablet
    @media only screen and (min-width: 700px) and (max-width: 820px) {
      height: 45px;
      width: 45px;
    }
  }
`;

export const LeftBtnCircle = styled.div`
  display: flex;
  height: 50px;
  width: 50px;
  margin: 0px 16px;
  align-self: center;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${(props) => (props.disabled ? "#EFF4FE" : "#bf2172")};
  border-radius: 100%;
  position: absolute;
  right: 10%;
  bottom: -20%;
  margin-right: 40px;
  cursor: pointer;

  @media only screen and (max-width: 820px) {
    bottom: -20%;
    right: unset;
    left: 0;
    height: 40px;
    width: 40px;

    // for tablet
    @media only screen and (min-width: 700px) and (max-width: 820px) {
      height: 45px;
      width: 45px;
    }
  }
`;

export const TeacherInfoQuote = styled.img`
  position: absolute;
  right: 10%;
  top: 8%;
  height: 80px;
  width: 80px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const TeacherImg = styled.img`
  height: 70px;
  width: 70px;
  border-radius: 100%;

  @media only screen and (max-width: 820px) {
    height: 60px;
    width: 60px;
  }
`;

export const TeacherPersonalInfo = styled.div`
  padding: 0px 12px;
  align-self: center;

  & > h4 {
    color: #000000;
    font-weight: 400;
    font-size: 1.1rem;
    padding: 4px 0;
  }

  & > p {
    color: #393939;
    font-size: 1.1rem;
    font-weight: 400;
    padding-bottom: 20px;
  }

  @media only screen and (max-width: 820px) {
    & > h4 {
      font-weight: 400;
      font-size: 1rem;
      padding: 4px 0;
    }

    & > p {
      font-size: 0.9rem;
      font-weight: 400;
      padding-bottom: 20px;
    }

    // for tablet
    @media only screen and (min-width: 700px) and (max-width: 820px) {
      & > h4 {
        font-size: 1.4rem;
      }

      & > p {
        font-size: 1.1rem;
      }
    }
  }
`;
