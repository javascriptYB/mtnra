export function cardContainerAnimation(ref, scrollData) {
  const { isDown, index } = scrollData;

  const keyFrame = [
    {
      transform: "translateX(100vw)",
    },
    {
      transform: "translateX(0%)",
    },
  ];

  const options = {
    duration: 1500,
    iterations: 1,
    fill: "forwards",
    easing: "ease-in-out",
  };

  if ((isDown || isDown === false) && index === 4) {
    ref.current.animate(keyFrame, options).play();
  } else if (isDown !== null) {
    ref.current.animate(keyFrame, { ...options, fill: "backwards" }).reverse();
  }
}

export function cardAnimation(ref, scrollData, duration) {
  const { isDown, index } = scrollData;

  const keyFrame = [
    {
      left: "40%",
    },
    {
      left: "0%",
    },
  ];

  const options = {
    duration: (duration + 1) * 700,
    iterations: 1,
    fill: "forwards",
    easing: "ease-in-out",
  };

  if ((isDown || !isDown) && index === 4) {
    ref.current.animate(keyFrame, options).play();
  }
}
