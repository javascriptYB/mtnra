import styled, { keyframes, css } from "styled-components";

const sectionAnimation = keyframes`
    100% {
        opacity: 1;
         background-color: transparent;
        //  background-color: yellow;
        height: 100%;
    }
`;

const sectionAnimationReverse = keyframes`
    %0 {

    }
    100% {
        opacity: 1;
        background-color: transparent;
        // background-color: yellow;
        height: 100%;
    }
`;

export const Section = styled.section`
  width: 100%;
  /* keep it for later */
  // height: 100vh;
  height: 100vh;
  opacity: 0;
  /* remove it later */
  position: absolute;
  bottom: 0;
  display: flex;
  flex-direction: column;
  // background-color: yellow;
  overflow: hidden;
  z-index: 9999;
  transform: translateY(100%);

  @media only screen and (max-width: 820px) {
    bottom: unset;
    position: unset;
    background: #eff4fe;
    height: unset;
    animation: unset;
    opacity: 1;
    height: 100%;
    transform: unset;s
  }
`;

export const Column = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  position: relative;

  @media only screen and (max-width: 820px) {
    align-items: flex-start;
    flex: unset;
    padding-top: 40px;
  }
`;

/* info */

const bgLogoContainerMoveUp = keyframes`
    100% {
        top: -100vh;
    }
`;

const bgLogoContainerMoveUpReverse = keyframes`
  0%{
    
  }
  100% {
    top: -100vh;
  }
`;

export const InfoContainer = styled.div`
  max-width: 700px;
  margin-left: 5%;
  top: 10%;
  transform: translateY(0px);
  position: relative;

  @media only screen and (max-width: 820px) {
    animation: unset;
  }
`;

const infoBgLogoAnimation = keyframes`
  100% {
    bottom: 0%;
  }
`;

export const InfoBgLogo = styled.div`
  bottom: -20%;
  position: relative;

  @media only screen and (max-width: 820px) {
    width: 100%;
    bottom: unset;
    position: unset;
    animation: unset;

    & > svg {
      width: 100%;
      max-height: 200px;
    }

    // for tablet
    @media only screen and (min-width: 700px) and (max-width: 820px) {
      & > svg {
        width: 100%;
        max-height: 300px;
      }
    }
  }
`;

const benfitsTextMoveUp = keyframes`
  0% {
    opacity: 1;
  }
  25% {
    opacity: 0.8;
  }
  50% {
    opacity: 0.6;
  }
  75% {
    opacity: 0.1;
  }
  100% {
    bottom: 100vh;
    opacity: 0;
  }
`;

const benfitsTextMoveUpReverse = keyframes`
  0% {
    opacity: 1;
  }
  25% {
    opacity: 0.8;
  }
  50% {
    opacity: 0.6;
  }
  80% {

  }
  75% {
    opacity: 0.1;
  }
  100% {
    bottom: 100vh;
    opacity: 0;
  }
`;

export const Benfits = styled.div`
  bottom: 45%;
  left: 5%;
  position: absolute;

  & > h2 {
    font-size: 2rem;
    color: #060a2b;
  }

  @media only screen and (max-width: 820px) {
    position: relative;
    bottom: 0;
    display: flex;
    align-items: center;
    left: 0;
    right: 0;
    margin-left: auto;
    margin-right: auto;
    animation: unset;

    & > h2 {
      font-size: 1.3rem;
      color: #060a2b;
      text-align: center;
    }

    // for tablet
    @media only screen and (min-width: 700px) and (max-width: 820px) {
      & > h2 {
        font-size: 2rem;
      }
    }
  }
`;

const moveDownGradientCol = keyframes`
    100% {
      bottom: -100vh;
    }
`;

const moveDownGradientColReverse = keyframes`
    0%{}
    100% {
      bottom: -100vh;
    }
`;

/* Gradient color */
export const GradientCol = styled.div`
  background-image: linear-gradient(
    rgba(239, 244, 254, 0),
    rgba(239, 244, 254, 0.9),
    rgba(239, 244, 254, 1)
  );
  bottom: 0%;
  max-height: 350px;

  flex: 1;
  // background-color: red;
  // height: 300px;
  display: flex;
  align-items: center;
  position: relative;

  @media only screen and (max-width: 820px) {
    background-image: unset;
    max-height: unset;
    animation: unset;
  }
`;

export const RowContainer = styled.div`
  flex: 1;
  flex-direction: row;
  display: flex;
  // position: absolute;
  width: 100%;
  margin-bottom: 5%;
  justify-content: flex-end;
  // background-color: green;
  @media only screen and (max-width: 820px) {
    position: relative;
    bottom: 0;
    margin-top: 20px;
  }
`;

export const Desc = styled.div`
  width: 50%;
  bottom: -40%;
  & > p {
    max-width: 600px;
  }
  & > p {
    color: #060a2b;
    font-size: 1rem;
  }

  // for mobile
  @media only screen and (max-width: 820px) {
    position: absolute;
    left: 0;
    right: 0;
    margin-left: auto;
    margin-right: auto;
    bottom: 26%;
    width: initial;
    width: 90%;
    max-width: 500px;
    text-align: center;

    & > p {
      max-width: unset;
    }

    & > p {
      font-size: 0.9rem;
    }

    // for tablet

    @media only screen and (min-width: 700px) and (max-width: 820px) {
      bottom: 26%;
      & > p {
        font-size: 1.09rem;
      }
    }
  }
`;

/* @keyframes circle-container-move-down {
  100% {
    top: 100vh;
  }
} */

export const CircleContainer = styled.div`
  position: absolute;
  left: -2%;
  top: 40%;

  @media only screen and (max-width: 820px) {
    position: unset;
    left: unset;
    top: unset;
    width: 100%;
    position: relative;
  }
  // animation: circle-container-move-down 2s ease-in-out 2s forwards;
`;

const circleContainerAnimation = keyframes`
    0% {
        transform: rotate(-45deg);
    }
    50% {
        transform: rotate(-90deg);
    }
    100% {
        transform: rotate(-135deg);
    }
`;

const circleContainerAnimationReverse = keyframes`
    0% {
        transform: rotate(-45deg);
    }
    50% {
        transform: rotate(-90deg);
    }
    75% {

    }
    100% {
        transform: rotate(-135deg);
    }
`;

const circleContainerAnimationReverse_1 = keyframes`
    0% {
        transform: rotate(45deg);
    }
    50% {
        transform: rotate(90deg);
    }
    75% {

    }
    100% {
        transform: rotate(135deg);
    }
`;

export const CircleIcon = styled.div`
  @media only screen and (max-width: 820px) {
    width: 100%;
    height: 400px;
    overflow: hidden;
    position: relative;
    display: flex;
    align-items: center;
    justify-content: center;
    animation: unset;

    & > svg {
      position: absolute;
      top: 0;
    }

    // for tablet
    @media only screen and (min-width: 700px) and (max-width: 820px) {
      & > svg {
        width: 150vw;
        height: 150vw;
        animation: unset;
      }
    }
  }
`;

/* arrow buttons */
export const LeftArrowBtn = styled.div`
  position: absolute;
  left: 0;
  right: 20%;
  margin-left: auto;
  margin-right: auto;
  height: 50px;
  width: 50px;
  top: 10%;
  border-radius: 100%;
  background-color: ${(props) => (props.disabled ? "#E2E2E2" : "#bf2172")};
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  z-index: 9999;

  @media only screen and (max-width: 820px) {
    bottom: 10%;
    top: unset;
  }
`;

export const RightArrowBtn = styled.div`
  position: absolute;
  left: 20%;
  right: 0;
  margin-left: auto;
  margin-right: auto;
  height: 50px;
  width: 50px;
  top: 10%;
  border-radius: 100%;
  background-color: ${(props) => (props.disabled ? "#E2E2E2" : "#bf2172")};
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  z-index: 9999;

  @media only screen and (max-width: 820px) {
    bottom: 10%;
    top: unset;
  }
`;

export const LocationPinContainer = styled.div`
  position: absolute;
  top: -24%;
  left: 0;
  right: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;

  & > h4 {
    padding: 12px 0;
    margin: 0;
    color: #bf2172;
    font-size: 1.5rem;
    margin-bottom: 5px;
  }

  @media only screen and (max-width: 820px) {
    margin-bottom: 30px;
    position: unset;
    & > h4 {
      padding-top: 0px;
      padding-bottom: 20px;
      margin: 0;
      color: #bf2172;
      font-size: 1.2rem;
    }

    // for tablet
    @media only screen and (min-width: 700px) and (max-width: 820px) {
      & > p:first-of-type {
        font-size: 1.1rem;
      }

      & > h1 {
        font-size: 2rem;
        line-height: 45px;
        padding-bottom: 20px;
      }
    }
  }
`;

export const LocationPinIcon = styled.div`
  width: 60px;
  height: 60px;
  margin-left: auto;
  margin-right: auto;
  border-radius: 50% 50% 50% 5%;
  // border-radius: 50% 50% 10% 0%;
  transform: rotate(-45deg);
  background-color: #bf2172;

  display: flex;
  align-items: center;
  justify-content: center;

  & > img {
    transform: rotate(45deg);
  }
`;

export const InfoText = styled.div`
  position: absolute;
  bottom: 50%;
  z-index: 9999;

  & > p:first-of-type {
    color: #060a2b;
    font-size: 1rem;
  }

  & > h1 {
    font-size: 2.2rem;
    line-height: 45px;
    color: #05071e;
    padding-bottom: 20px;
  }

  & > h1 > span {
    color: #bf2172;
  }

  @media only screen and (max-width: 820px) {
    position: absolute;
    top: 2%;
    z-index: 9999;

    & > p:first-of-type {
      color: #060a2b;
      font-size: 0.9rem;
    }

    & > h1 {
      font-size: 1.3rem;
      line-height: 30px;
      padding-bottom: 20px;
      color: #05071e;
    }

    & > h1 > span {
      color: #bf2172;
    }

    // for tablet
    @media only screen and (min-width: 700px) and (max-width: 820px) {
      & > p:first-of-type {
        font-size: 1.1rem;
      }

      & > h1 {
        font-size: 2rem;
        line-height: 45px;
        padding-bottom: 20px;
      }
    }
  }
`;
