import { useEffect, useRef, useState } from "react";
import {
  ArrowIcon,
  ChillIcon,
  CoachingIcon,
  DottedCircleIcon,
  GroupIcon,
  HomeworkIcon,
  IndividualIcon,
  LeftArrowIcon,
  RightArrowIcon,
} from "../../../assets/icons";
import BackgroundLogo from "../../../assets/icons/BackgroundLogo";
import Seminar from "../../../assets/icons/Seminar";
import useDevice from "../../hooks/useDevice";
import useScroll from "../../hooks/useScroll";
import {
  animateDescriptionText,
  benfitsAnimation,
  circleIconAnimation,
  gradientColAnimation,
  infoBgLogoAnimation,
  infoContainerAnimation,
  mainSectionAnimation,
} from "./animations";

import {
  Benfits,
  CircleContainer,
  CircleIcon,
  Column,
  Desc,
  GradientCol,
  InfoBgLogo,
  InfoContainer,
  InfoText,
  LeftArrowBtn,
  LocationPinContainer,
  LocationPinIcon,
  RightArrowBtn,
  RowContainer,
  Section,
} from "./styles";

import Image from "next/image";

const featuresData = [
  {
    title: "Autonomie",
    icon: <ArrowIcon />,
    description:
      "Chez XII l’autonomie de nos élèves est une valeur fondamentale. En plus de garantir\
       une amélioration des notes sur le court terme en renforçant les connaissances fondamentales,\
        nous mettons un focus particulier sur les « soft skills » des élèves afin de permettre une réussite qui\
         va au-delà du prochain Devoir Surveillé ou même du Baccalauréat. En choisissant XII vous offrez à votre enfant\
           les outils adéquats pour affronter le monde de demain",
  },
  {
    title: "Stages de vacances",
    icon: <ChillIcon />,
    description:
      "Pendant les vacances scolaires, nos élèves auront la possibilité\
       de suivre des stages permettant de consolider leur connaissance,\
       combler leurs lacunes et se préparer pour des examens spécifiques\
        (IELTS, TOEFL, etc.). Nos stages seront l’occasion d’initier de nouvelles\
         vocations grâce à de nouvelles formations telles que le codage et la programmation informatique ou la gestion du stress",
  },
  {
    title: "Séminaires",
    icon: <Seminar />,
    description:
      "Des interventions régulières seront programmées afin de présenter à nos élèves différents profils professionnels.\
       Ils pourront ainsi être en contact direct avec différents métiers et parcours scolaires. Nos intervenants apporteront un aperçu du marché de \
       l’emploi et les dernières nouveautés sur les programmes de formation dans le monde",
  },
  {
    title: "L'aide au devoir",
    icon: <HomeworkIcon />,
    description:
      "Un environnement dédié au travail dans une ambiance studieuse et efficace\
       pour accompagner les collégiens dans le travail de tous les jours.\
        Ils pourront développer des méthodes de travail et d’organisation efficientes\
         dans un cadre favorisant l’autonomie et l’accomplissement personnel",
  },
  {
    title: "Cours particuliers individuelles",
    icon: <IndividualIcon />,
    description:
      "Un duo professeur-élève est créé pour une approche entièrement dédiée.\
       Le tandem progresse en prenant en compte des axes d’évolution et des points\
        forts pour une approche chirurgicale. Cette formule permet une flexibilité totale dans le choix des créneaux et des matières",
  },
  {
    title: "Cours particuliers en petits groupes",
    icon: <GroupIcon />,
    description:
      "Les petits groupes permettent de donner une nouvelle dimension\
       au travail personnel des élèves et de créer des synergies dans l’émulation.\
        Un maximum de 6 élèves est garanti pour une meilleure optimisation du travail en groupe",
  },
  {
    title: "Coaching scolaire",
    icon: <CoachingIcon />,
    description:
      "L’élève doit être pris dans sa globalité. Nos coachs, experts en éducation\
       et nos professeurs développent une approche personnalisée qui permet\
        à celui-ci de révéler tout son potentiel et de réussir son projet d’étude.\
         Nous participons à la construction d’une personnalité forte et résiliente pour favoriser la réussite au-delà de l’école",
  },
];

// const Icon = (props: any) => {
//   return (

//   );
// };

interface FeaturesProps {
  data: {
    description: string;
    subtitle: string;
    title: string;
    title_2: string;
    sliders: [
      {
        title: string;
        description: string;
        image: { uri: string; alt: string };
      }
    ];
  };
}

const Features = (props: FeaturesProps) => {
  const scrollData = useScroll();
  const device = useDevice();
  const [active, setActive] = useState(0);
  const mainSectionRef = useRef(null);
  const infoContainerRef = useRef(null);
  const infoBgLogoRef = useRef(null);
  const benfitsRef = useRef(null);
  const gradientColRef = useRef(null);
  const circleIconRef = useRef(null);
  const descriptionRef = useRef(null);

  const { data } = props;

  useEffect(() => {
    if (device.isDesktop) {
      mainSectionAnimation(mainSectionRef, scrollData);
      infoContainerAnimation(infoContainerRef, scrollData);
      infoBgLogoAnimation(infoBgLogoRef, scrollData);
      benfitsAnimation(benfitsRef, scrollData);
      gradientColAnimation(gradientColRef, scrollData);
      circleIconAnimation(circleIconRef, scrollData, undefined);
    }
  }, [scrollData]);

  const circleAnimationHandler = (direction: string) => {
    setTimeout(() => {
      if (device.isDesktop)
        circleIconAnimation(circleIconRef, scrollData, direction);
      animateDescriptionText(descriptionRef);
    }, 1);
  };

  return (
    <Section ref={mainSectionRef}>
      <Column>
        <InfoContainer ref={infoContainerRef}>
          <InfoBgLogo ref={infoBgLogoRef}>
            <BackgroundLogo />
          </InfoBgLogo>
          <InfoText>
            <h1>
              {data?.title} <span>{data?.title_2}</span>
            </h1>
            <p>{data?.description}</p>
          </InfoText>
        </InfoContainer>
      </Column>
      <Benfits ref={benfitsRef}>
        <h2>{data?.subtitle}</h2>
      </Benfits>
      {data?.sliders?.length > 0 &&
        data?.sliders.map((feature, index) => {
          if (index === active) {
            return (
              <GradientCol ref={gradientColRef} key={index}>
                <RowContainer>
                  <Desc ref={descriptionRef}>
                    <p>{feature.description}</p>
                  </Desc>

                  <CircleContainer>
                    <LocationPinContainer>
                      <h4>{feature.title}</h4>
                      <LocationPinIcon>
                        <img
                          src={feature?.image?.uri}
                          alt={feature?.image?.alt}
                        />
                      </LocationPinIcon>
                    </LocationPinContainer>

                    <RightArrowBtn
                      onClick={(e) => {
                        e.preventDefault();
                        if (featuresData.length - 1 > index) {
                          setActive(index + 1);

                          circleAnimationHandler("right");
                        }
                      }}
                      disabled={featuresData.length - 1 === index}
                    >
                      <RightArrowIcon
                        disabled={featuresData.length - 1 === index}
                      />
                    </RightArrowBtn>

                    <LeftArrowBtn
                      onClick={(e) => {
                        e.preventDefault();
                        if (index > 0) {
                          setActive(index - 1);

                          circleAnimationHandler("left");
                        }
                      }}
                      disabled={index === 0}
                    >
                      <LeftArrowIcon disabled={index === 0} />
                    </LeftArrowBtn>

                    <CircleIcon ref={circleIconRef}>
                      <DottedCircleIcon />
                    </CircleIcon>
                  </CircleContainer>
                </RowContainer>
              </GradientCol>
            );
          }
        })}
    </Section>
  );
};

export default Features;
