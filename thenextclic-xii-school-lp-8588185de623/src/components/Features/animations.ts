const options = {
  duration: 1500,
  iterations: 1,
  fill: "forwards",
  easing: "ease-in-out",
};

export function mainSectionAnimation(ref, scrollData) {
  const { isDown, index } = scrollData;

  const positionKeyFrame = [
    { transform: "translateY(100%)", opacity: 0 },
    { transform: "translateY(0%)", opacity: 1 },
  ];

  if (isDown && index === 1)
    ref.current.animate(positionKeyFrame, options).play();
  else if (isDown === false && index === 0)
    ref.current
      .animate(positionKeyFrame, { ...options, fill: "backwards" })
      .reverse();
  else if (isDown === null && index === 0)
    ref.current
      .animate(positionKeyFrame, {
        ...options,
        fill: "backwards",
        iterations: 0,
      })
      .reverse();
}

export function infoContainerAnimation(ref, scrollData) {
  const { isDown, index } = scrollData;

  const positionKeyFrame = [
    { transform: "translateY(0%)" },
    { transform: "translateY(-100vh)" },
  ];

  if (isDown && index === 2)
    ref.current.animate(positionKeyFrame, options).play();
  else if (isDown === false && index === 1)
    ref.current
      .animate(positionKeyFrame, { ...options, fill: "backwards" })
      .reverse();
  else if (isDown === null && index === 0)
    ref.current
      .animate(positionKeyFrame, {
        ...options,
        fill: "backwards",
        iterations: 0,
      })
      .reverse();
}

export function infoBgLogoAnimation(ref, scrollData) {
  const { isDown, index } = scrollData;

  const positionKeyFrame = [
    { transform: "translateY(0%)" },
    { transform: "translateY(-100vh)" },
  ];

  if (isDown && index === 2)
    ref.current.animate(positionKeyFrame, options).play();
  else if (isDown === false && index === 1)
    ref.current
      .animate(positionKeyFrame, { ...options, fill: "backwards" })
      .reverse();
  else if (isDown === null && index === 0)
    ref.current
      .animate(positionKeyFrame, {
        ...options,
        fill: "backwards",
        iterations: 0,
      })
      .reverse();
}

export function benfitsAnimation(ref, scrollData) {
  const { isDown, index } = scrollData;

  const positionKeyFrame = [
    { transform: "translateY(0%)" },
    { transform: "translateY(-100vh)" },
  ];

  if (isDown && index === 2)
    ref.current.animate(positionKeyFrame, options).play();
  else if (isDown === false && index === 1)
    ref.current
      .animate(positionKeyFrame, { ...options, fill: "backwards" })
      .reverse();
  else if (isDown === null && index === 0)
    ref.current
      .animate(positionKeyFrame, {
        ...options,
        fill: "backwards",
        iterations: 1,
      })
      .reverse();
}

export function gradientColAnimation(ref, scrollData) {
  const { isDown, index } = scrollData;

  const positionKeyFrame = [
    { transform: "translateY(0%)" },
    { transform: "translateY(100vh)" },
  ];

  if (isDown && index === 2)
    ref.current.animate(positionKeyFrame, options).play();
  else if (isDown === false && index === 1)
    ref.current
      .animate(positionKeyFrame, { ...options, fill: "backwards" })
      .reverse();
  else if (isDown === null && index === 0)
    ref.current
      .animate(positionKeyFrame, {
        ...options,
        fill: "backwards",
        iterations: 1,
      })
      .reverse();
}

export function circleIconAnimation(ref, scrollData, direction) {
  const { isDown, index } = scrollData;

  let keyFrame = [
    { transform: "rotate(0deg)" },
    { transform: "rotate(-135deg)" },
  ];

  if (direction) keyFrame[1] = { transform: "rotate(-45deg)" };

  if ((isDown && index === 2) || direction === "right") {
    ref.current.animate(keyFrame, options).play();
  } else if ((isDown === false && index === 1) || direction === "left")
    ref.current.animate(keyFrame, { ...options, fill: "backwards" }).reverse();
}

export function animateDescriptionText(ref) {
  let keyFrame = [
    { transform: "translateY(50px)", opacity: 0 },
    { transform: "translateY(-10px)", opacity: 1 },
  ];

  ref.current.animate(keyFrame, options).play();
}
