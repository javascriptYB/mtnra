const options = {
  duration: 1500,
  iterations: 1,
  fill: "forwards",
  easing: "ease-in-out",
};

export function mailImgAnimation(ref, scrollData) {
  const { isDown, index } = scrollData;

  const keyFrame = [
    { transform: "rotate(-90deg)" },
    { transform: "rotate(0deg)" },
  ];

  if (isDown && index === 5) {
    ref.current.animate(keyFrame, { ...options }).play();
  } else if (isDown === false && index === 4)
    ref.current.animate(keyFrame, { ...options, fill: "backwards" }).reverse();
}

export function circleAnimation(ref, scrollData) {
  const { isDown, index } = scrollData;

  const keyFrame = [
    { transform: "scale(1.5) translateY(0%) translateX(0%)" },
    { transform: "translateY(80%) translateX(80%)" },
  ];

  if (isDown && index === 5) {
    ref.current.animate(keyFrame, { ...options }).play();
  } else if (isDown === false && index === 4)
    ref.current.animate(keyFrame, { ...options, fill: "backwards" }).reverse();
}
