import styled, { keyframes, css } from "styled-components";

export const Container = styled.section`
  height: 100vh;
  width: 100vw;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  overflow: hidden;
  background-color: #f9fcff;
  position: relative;

  @media only screen and (max-width: 820px) {
    height: 100%;
    width: 100%;
    display: flex;
    flex-direction: column;
  }
`;

// const contactUsContainerShow = keyframes`
//     100% {
//       height: 100%;
//       opacity: 1;
//     }
//   `;

// animation: ${contactUsContainerShow} 2s ease-in-out forwards;

export const ContactUsContainer = styled.div`
  display: flex;
  flex-direction: column;
  background-color: #ffffff;
  // keep it later;
  height: 0%;

  // remove it later
  height: 100%;
  // keep it later
  opacity: 0;
  // remove it later
  opacity: 1;

  width: 100%;
  align-items: center;
  bottom: 0%;
  position: absolute;

  @media only screen and (max-width: 820px) {
    height: 100%;
    bottom: unset;
    position: relative;
  }
`;

export const ContactUsForm = styled.form`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  width: 80%;
  max-width: 800px;
  text-align: center;
  position: relative;
  margin-bottom: 10%;

  & > h1 {
    color: #060a2b;
    font-size: 3.8rem;
    padding: 20px 0;
    font-weight: bold;
  }

  & > p {
    max-width: 50%;
    font-size: 1.1rem;
    color: #060a2b;
    font-weight: 400;
    padding-bottom: 10px;
    margin-bottom: 20px;
  }

  & > button {
    border: none;
    padding: 14px 80px;
    border-radius: 22px;
    background-color: ${({ btnDisabled }) =>
      btnDisabled ? "rgba(226, 226, 226, 1)" : "#BF2172"};
    color: ${({ btnDisabled }) =>
      btnDisabled ? "rgba(133, 133, 133, 1)" : "#fff"};
    font-size: 1rem;
    margin-top: 20px;
    cursor: pointer;
  }

  & > input {
    width: 80%;
    max-width: 500px;
    min-width: 200px;
    padding: 12px 20px;
    margin: 12px 0;
    border: none;
    border-bottom: 1px solid #e2e2e2;
    border-radius: 4px;
    font-size: 1.1rem;
    font-weight: 500;
    color: #707070;
    margin: 8px 0;
  }

  & > input:focus {
    outline: 2px solid #be2172;
  }

  & > textarea {
    width: 80%;
    max-width: 500px;
    min-width: 200px;
    padding: 12px 20px;
    margin: 24px 0;
    border: none;
    border-bottom: 1px solid #e2e2e2;
    border-radius: 4px;
    margin: 5px 0;
    font-size: 1.1rem;
    font-weight: 500;
    color: #707070;
  }

  & > textarea:focus {
    outline: 2px solid #be2172;
  }

  @media only screen and (max-width: 820px) {
    flex: 1;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    width: 100%;
    max-width: 800px;
    text-align: center;
    position: relative;
    margin: 40px 0;

    & > h1 {
      font-size: 1.5rem;
      padding: 20px 0;
      font-weight: bold;
    }

    & > p {
      max-width: 80%;
      font-size: 0.8rem;
    }

    & > button {
      padding: 14px 80px;
      font-size: 0.9rem;
      margin-top: 20px;
    }

    & > input[type="text"] {
      max-width: 500px;
      min-width: 200px;
      padding: 12px 20px;
      margin: 12px 0;
      border: none;
      border-bottom: 1px solid #e2e2e2;
      border-radius: 4px;
      font-size: 1rem;
      font-weight: 500;
      color: #707070;
      margin: 8px 0;
    }

    & > textarea {
      width: 80%;
      max-width: 500px;
      min-width: 200px;
      padding: 12px 20px;
      margin: 24px 0;
      border: none;
      border-bottom: 1px solid #e2e2e2;
      border-radius: 4px;
      margin: 5px 0;
      font-size: 1rem;
    }

    // for tablet
    @media only screen and (min-width: 700px) and (max-width: 820px) {
      margin: 40px 0;
      & > h1 {
        font-size: 2rem;
      }

      & > p {
        max-width: 50%;
        font-size: 1.1rem;
        margin-bottom: 20px;
      }

      & > button {
        font-size: 1.1rem;
      }

      & > input[type="text"] {
        font-size: 1.1rem;
      }

      & > textarea {
        font-size: 1.1rem;
      }
    }
  }
`;

const mailImgRotate = keyframes`
    100% {
      transform: rotate(0deg);
    }
`;

const mailImgRotateReverse = keyframes`
    0%{}
    100% {
      transform: rotate(0deg);
    }
`;

export const MailImg = styled.img`
  position: absolute;
  right: -5%;
  top: 20%;
  transform: rotate(-90deg);

  @media only screen and (max-width: 820px) {
    right: -5%;
    top: -0%;
    height: 15%;
    transform: rotate(0deg);
    animation: unset;
  }
`;

const formCircleShow = keyframes`
    100% {
      top: -20%;
      left: -8%;
    }
`;

const formCircleShowReverse = keyframes`
    0%{}
    100% {
      top: -20%;
      left: -8%;
    }
`;

export const FormCircle = styled.div`
  background-color: #f2f6fe;
  height: 40vh;
  width: 40vh;
  border-radius: 100%;
  position: absolute;
  top: -50vh;
  left: -50vh;
  transform: scale(1.5);

  @media only screen and (max-width: 820px) {
    height: 20vw;
    width: 20vw;
    position: absolute;
    left: -17%;
    top: 5%;
    transform: scale(1.5);
    animation: unset;
  }
`;
