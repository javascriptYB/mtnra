import { forwardRef, useCallback, useEffect, useRef, useState } from "react";
import useDevice from "../../hooks/useDevice";
import useScroll from "../../hooks/useScroll";
import sectionScroll from "../../utils";
import Footer from "../Footer";
import { circleAnimation, mailImgAnimation } from "./animations";
import {
  ContactUsContainer,
  ContactUsForm,
  Container,
  FormCircle,
  MailImg,
} from "./styles";

const ContactUs = forwardRef((props: { prevSection: any }, ref: any) => {
  const mailImgRef = useRef(null);
  const circleRef = useRef(null);
  const scrollData = useScroll();
  const device = useDevice();
  const [input, onChangeInput] = useState({
    email: "",
    objet: "",
    message: "",
  });

  const onChange = (event) => {
    onChangeInput((oldState) => {
      return { ...oldState, [event.target.name]: event.target.value };
    });
  };

  const onClick = (event) => event.preventDefault();

  useEffect(() => {
    if (device.isDesktop) {
      if (scrollData.index === 4 && !scrollData.isDown) {
        sectionScroll(props.prevSection.current, 1500);
      }
      mailImgAnimation(mailImgRef, scrollData);
      circleAnimation(circleRef, scrollData);
    }
  }, [scrollData]);

  const isDisabled = useCallback(() => {
    if (input.email.length && input.objet.length && input.message.length)
      return false;
    return true;
  }, [input.email.length, input.objet.length, input.message.length]);

  return (
    <Container ref={ref}>
      <ContactUsContainer>
        <MailImg src="img/mail.png" ref={mailImgRef} />

        <FormCircle ref={circleRef} />

        <ContactUsForm btnDisabled={isDisabled()}>
          <h1>Contactez-nous</h1>
          <p>
            Notre centre d’aide est toujours à jour et disponible pour répondre
            à vos questions
          </p>
          <input
            type="text"
            placeholder="Adresse mail"
            name="email"
            onChange={onChange}
          />
          <input
            type="text"
            placeholder="Objet"
            name="objet"
            onChange={onChange}
          />
          <textarea
            placeholder="Message"
            rows={5}
            name="message"
            onChange={onChange}
          />
          <button disabled={isDisabled()} onClick={onClick}>
            Envoyer
          </button>
        </ContactUsForm>
      </ContactUsContainer>
      <Footer />
    </Container>
  );
});

export default ContactUs;
