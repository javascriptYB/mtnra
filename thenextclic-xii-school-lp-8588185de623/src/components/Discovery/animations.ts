const options = {
  duration: 1500,
  iterations: 1,
  fill: "forwards",
  easing: "ease-in-out",
};

export function videoContainerAnimation(ref, scrollData) {
  const { isDown, index } = scrollData;

  const keyFrame = [
    { transform: "translateY(100%)", opacity: 0 },
    { transform: "translateY(0%)", opacity: 1 },
  ];

  if (isDown && index === 2) ref.current.animate(keyFrame, options).play();
  else if (isDown === false && index === 1)
    ref.current.animate(keyFrame, { ...options, fill: "backwards" }).reverse();
  else if (isDown === null && index === 0)
    ref.current
      .animate(keyFrame, { ...options, fill: "backwards", iterations: 0 })
      .reverse();
}
