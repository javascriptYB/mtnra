import styled, { keyframes, css } from "styled-components";

const videoContainerAnimation = keyframes`
  100% {
    opacity: 1;
    background-color: transparent;
    // background-color: green;
    height: 100%;
  }
`;

const videoContainerAnimationReverse = keyframes`
  %0{}
  100% {
    opacity: 1;
    background-color: transparent;
    // background-color: green;
    height: 100%;
  }
`;

export const VideoContainer = styled.section`
  width: 100%;
  height: 100%;
  opacity: 0;
  position: absolute;
  display: flex;
  bottom: 0;
  /* flex-direction: row; */
  justify-content: center;
  align-items: center;
  z-index: 9999;
  overflow: hidden;
  transform: translateY(100%);

  @media only screen and (max-width: 820px) {
    background: #f9f5ee;
    opacity: 1;
    position: unset;
    bottom: unset;
    height: 100%;
    align-items: flex-start;
    flex-direction: column;
    position: relative;
    // height: 100vh;
    // height: unset;
    animation: unset;
    overflow-y: scroll;
    transform: unset;
  }
`;

export const VideoInfo = styled.div`
  max-width: 580px;
  display: flex;
  justify-content: center;
  flex-direction: column;
  & > h1 {
    color: rgba(0, 0, 0, 1);
    font-weight: bold;
    font-size: 2.5rem;
    line-height: 45px;
    padding-bottom: 30px;
  }
  & > h1 > span {
    color: rgba(191, 33, 114, 1);
    font-weight: 100;
  }
  & > p {
    color: rgba(6, 10, 43, 1);
    font-size: 1rem;
    line-height: 1.3rem;
    padding-bottom: 15px;
  }

  @media only screen and (max-width: 820px) {
    display: flex;
    justify-content: flex-start;
    flex-direction: column;
    width: 90vw;
    max-width: unset;
    padding: 20px;
    margin-top: 40px;

    & > h1 {
      color: rgba(0, 0, 0, 1);
      font-weight: bold;
      font-size: 1.3rem;
      padding-bottom: 5%;
    }
    & > h1 > span {
      color: rgba(191, 33, 114, 1);
      font-weight: 100;
    }
    & > p {
      color: rgba(6, 10, 43, 1);
      font-size: 0.9rem;
      line-height: 1.1rem;
      padding-bottom: 15px;
    }

    // for tablet
    @media only screen and (min-width: 700px) and (max-width: 820px) {
      & > h1 {
        font-size: 2rem;
      }
      & > p {
        font-size: 1.1rem;
        line-height: 25px;
        max-width: 500px;
      }
    }
  }
`;

export const VideoFrame = styled.div`
  background: transparent;
  border-radius: 20px;
  opacity: 1;
  max-height: 250px;
  height: 26vh;
  width: 100%;
  align-items: "center";
  justify-content: "center";
  display: flex;
  position: relative;

  @media only screen and (max-width: 820px) {
    max-height: 300px;
    height: 30vh;

    // for tabelt
    @media only screen and (min-width: 700px) and (max-width: 820px) {
      max-height: 500px;
      height: 30vh;
    }
  }
`;

export const FlexItem = styled.div`
  display: flex;
  flex: 1;
  align-items: center;
  justify-content: center;
  /* background-color: darkgreen; */
`;

const bottomCircleMoveUp = keyframes`
    100% {
      top: 90%;
    }
  `;

// animation: ${bottomCircleMoveUp} 2s ease-in-out 2s forwards;

export const BottomCircle = styled.div`
  height: 20vw;
  width: 20vw;
  background-image: linear-gradient(
    to right,
    rgba(32, 50, 212, 1),
    rgba(32, 45, 165, 1)
  );
  position: absolute;
  top: 100%;
  border-radius: 100%;
  @media only screen and (max-width: 820px) {
    display: none;
  }
`;

// mobile
export const SmallCircle = styled.div`
  @media only screen and (max-width: 820px) {
    height: 20vw;
    width: 20vw;
    border-radius: 100%;
    background-image: linear-gradient(to right, #fa6d30, #e4067e);
    position: absolute;
    top: -6%;
    left: 70%;
  }
`;

export const AssetContainer = styled.div`
  display: none;
  @media only screen and (max-width: 820px) {
    position: relative;
    height: 350px;
    width: 100%;
    overflow: hidden;
    display: flex;

    // for tabelt
    @media only screen and (min-width: 700px) and (max-width: 820px) {
      height: 600px;
      width: 100%;
    }
  }
`;

export const BigCircle = styled.div`
  @media only screen and (max-width: 820px) {
    width: 20vw;
    height: 20vw;
    position: absolute;
    bottom: 0%;
    left: -10%;
    border-radius: 100%;
    background-image: linear-gradient(to right, #2032d4, #202da5);
    transform: scale(1.5);
  }
`;

export const ChildImg = styled.img`
  @media only screen and (max-width: 820px) {
    height: 95%;
    position: absolute;
    bottom: 2%;
    right: 5%;
  }
`;
