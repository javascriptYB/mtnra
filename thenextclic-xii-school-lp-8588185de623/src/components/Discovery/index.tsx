import useScroll from "../../hooks/useScroll";
import {
  AssetContainer,
  BigCircle,
  BottomCircle,
  ChildImg,
  FlexItem,
  SmallCircle,
  VideoContainer,
  VideoFrame,
  VideoInfo,
} from "./styles";
import { Ref, useEffect, useRef, useState } from "react";
import sectionScroll from "../../utils";
import useDevice from "../../hooks/useDevice";
import dynamic from "next/dynamic";
import { videoContainerAnimation } from "./animations";

const ReactPlayer = dynamic(() => import("react-player/lazy"), { ssr: false });

interface DiscoveryProps {
  nextSection: React.RefObject<HTMLInputElement>;
  data: {
    description: string;
    subtitre: string;
    title: string;
    title_2: string;
    video: string;
  };
}

const Discovery = (props: DiscoveryProps) => {
  const scrollData = useScroll();
  const device = useDevice();
  const videoContainerRef = useRef();

  const { data } = props;

  useEffect(() => {
    if (device.isDesktop) {
      if (scrollData.index === 3 && scrollData.isDown) {
        sectionScroll(props.nextSection.current, 1500);
      }
      videoContainerAnimation(videoContainerRef, scrollData);
    }
  }, [scrollData]);

  return (
    <VideoContainer ref={videoContainerRef}>
      <FlexItem>
        <VideoInfo>
          <h1>
            {data?.title} <span>{data?.title_2}</span>
            <br /> {data?.subtitre}
          </h1>
          <p>{data?.description}</p>
          <VideoFrame>
            <ReactPlayer
              height={"100%"}
              width={"100%"}
              url={data?.video}
              style={{
                position: "absolute",
                left: 0,
                top: 0,
                borderRadius: "20px",
              }}
            />
          </VideoFrame>
          <BottomCircle />
        </VideoInfo>
      </FlexItem>

      {/* this will be displayed just for mobile */}
      <SmallCircle />

      <AssetContainer>
        <ChildImg src="img/kid-2.png" />
        <BigCircle />
      </AssetContainer>

      <FlexItem />
    </VideoContainer>
  );
};

export default Discovery;
