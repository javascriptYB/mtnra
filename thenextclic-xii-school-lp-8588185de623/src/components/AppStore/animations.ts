const options = {
  duration: 1500,
  iterations: Infinity,
  direction: "alternate",
  easing: "ease-in-out",
};

export function imgOneAnimation(ref, scrollData) {
  const moveDownKeyFrame = [
    { transform: "translateY(10px)" },
    { transform: "translateY(10px)" },
  ];

  ref.current.animate(moveDownKeyFrame, options).play();
}

export function imgTwoAnimation(ref, scrollData) {
  const moveDownKeyFrame = [
    { transform: "translateY(-10px)" },
    { transform: "translateY(10px)" },
  ];

  ref.current
    .animate(moveDownKeyFrame, { ...options, direction: "alternate-reverse" })
    .play();
}
