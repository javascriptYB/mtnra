import { forwardRef, useEffect, useRef } from "react";
import { CheckIcon } from "../../../assets/icons";
import useDevice from "../../hooks/useDevice";
import useScroll from "../../hooks/useScroll";
import sectionScroll from "../../utils";
import scroll from "../../utils";
import { imgOneAnimation, imgTwoAnimation } from "./animations";
import {
  LeftCircle,
  LeftMobilePic,
  MobileImgsContainer,
  RightMobilePic,
  RowItem,
  StoreBottomCircle,
  StoreContainer,
  StoreInfo,
  StoreRow,
} from "./styles";

interface AppStoreProps {
  prevSection: React.RefObject<HTMLInputElement>;
  nextSection: React.RefObject<HTMLInputElement>;
  data: {
    description: string;
    title: string;
    google_play: string;
    app_store: string;
    avantages: [];
  };
}

const AppStore = forwardRef((props: AppStoreProps, ref: any) => {
  const scrollData = useScroll();
  const device = useDevice();
  const imgOneRef = useRef(null);
  const imgTwoRef = useRef(null);

  const { data } = props;

  useEffect(() => {
    if (device?.isDesktop) {
      if (scrollData.index === 4 && scrollData.isDown) {
        sectionScroll(props.nextSection.current, 2000);
      }
    }
    imgOneAnimation(imgOneRef, scrollData);
    imgTwoAnimation(imgTwoRef, scrollData);
  }, [scrollData]);

  return (
    <StoreContainer ref={ref}>
      <StoreRow>
        <RowItem>
          <StoreInfo>
            <h1>{data?.title}</h1>
            <p>{data?.description}</p>
            <ol>
              {data?.avantages.map((item, index) => {
                return (
                  <div key={index}>
                    <CheckIcon />
                    <li>{item}</li>
                  </div>
                );
              })}
            </ol>
            <p>Télécharger l'application</p>
            <div>
              <a href={data?.google_play} target={"_blank"}>
                <img src="./img/google-play.png" />
              </a>
              <a href={data?.app_store} target={"_blank"}>
                <img src="./img/apple-store.png" />
              </a>
            </div>
          </StoreInfo>
        </RowItem>

        <RowItem>
          <MobileImgsContainer>
            <RightMobilePic ref={imgOneRef} src="img/phone-1.png" />
            <LeftMobilePic ref={imgTwoRef} src="img/phone-2.png" />
          </MobileImgsContainer>
        </RowItem>
      </StoreRow>
      <LeftCircle />
      <StoreBottomCircle />
    </StoreContainer>
  );
});

export default AppStore;
