import styled, { keyframes, css } from "styled-components";

export const StoreContainer = styled.section`
  height: 100vh;
  width: 100vw;
  background-color: #f9fcff;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  overflow: hidden;

  @media only screen and (max-width: 820px) {
    background: #ffffff;
    opacity: 1;
    height: 100%;
    align-items: flex-start;
    width: 100%;
    display: block;
    margin: auto;
    // overflow-y: scroll;
  }
`;

export const StoreRow = styled.div`
  display: flex;
  flex-direction: row;
  width: 80%;
  align-items: center;
  justify-content: center;
  height: 60%;
  position: relative;

  @media only screen and (max-width: 820px) {
    margin: auto;
    margin-top: 0;
    margin-bottom: 0;
    width: 90%;
    flex-direction: column-reverse;

    & > div:first-of-type {
      justify-content: center;
      display: flex;
      margin-top: 40px;
      width: 100%;
    }
  }
`;

export const RowItem = styled.div`
  flex: 1;
  justify-content: center;
  align-items: center;
  height: 100%;
  width: 100%;
`;

export const StoreInfo = styled.div`
  max-width: 700px;

  & > h1 {
    color: #000000;
    font-size: 2.5rem;
    font-weight: bold;
    font-weight: 700;
    padding: 0;
    margin: 0;
    margin-bottom: 25px;
  }

  & > p:first-of-type {
    color: #060a2b;
    font-size: 1rem;
    font-weight: 400;
  }

  & > p:last-of-type {
    color: #393939;
    font-size: 1rem;
    font-weight: 500;
    margin-bottom: 10px;
  }

  & > ol {
    margin: 0px;
    padding: 0px;
    list-style-type: none;
    margin-bottom: 30px;
  }

  & > ol > div {
    display: flex;
    align-items: center;
    padding-top: 20px;
  }

  & > ol > div > svg {
    margin-right: 5px;
  }

  & > ol > div > li {
    color: #060a2b;
    font-weight: bold;
    font-size: 0.9rem;
  }

  & > div > a > img:first-of-type {
    margin-right: 10px;
  }

  & > div > a > img {
    cursor: pointer;
  }

  @media only screen and (max-width: 820px) {
    max-width: 90%;
    max-width: 310px;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;

    & > h1 {
      color: #000000;
      font-size: 1.3rem;
      font-weight: bold;
      font-weight: 700;
      padding: 0;
      margin: 0;
      margin-bottom: 25px;
      text-align: center;
    }

    & > p:first-of-type {
      color: #060a2b;
      font-size: 0.9rem;
      font-weight: 400;
      text-align: center;
    }

    & > p:last-of-type {
      color: #393939;
      font-size: 0.9rem;
      font-weight: 500;
      margin-bottom: 10px;
    }

    & > ol {
      margin: 0px;
      padding: 0px;
      list-style-type: none;
      margin-bottom: 30px;
    }

    & > ol > div {
      display: flex;
      align-items: center;
    }

    & > div {
      padding-bottom: 20px;
    }

    & > ol > div > li {
      color: #060a2b;
      font-weight: bold;
      font-size: 0.9rem;
    }

    // for tablet
    @media only screen and (min-width: 700px) and (max-width: 820px) {
      max-width: 90%;
      max-width: 450px;
      display: flex;
      align-items: center;
      justify-content: center;
      flex-direction: column;

      & > h1 {
        font-size: 2rem;
      }

      & > p:first-of-type {
        font-size: 1.1rem;
      }

      & > p:last-of-type {
        font-size: 1.1rem;
      }

      & > ol > div > li {
        font-size: 1.1rem;
      }
    }
  }
`;

export const LeftCircle = styled.div`
  height: 20vw;
  width: 20vw;
  background-color: #fa6d30;
  border-radius: 100%;
  left: -16%;
  top: 5%;
  position: absolute;

  @media only screen and (max-width: 820px) {
    left: -20%;
    width: 30vw;
    height: 30vw;
  }
`;

export const StoreBottomCircle = styled.div`
  height: 20vw;
  width: 20vw;
  background-color: #ffbb44;
  border-radius: 100%;
  right: 5%;
  bottom: -28%;
  position: absolute;

  @media only screen and (max-width: 820px) {
    width: 30vw;
    height: 30vw;
    transform: scale(1.5);
    right: unset;
    left: 96%;
    bottom: 0;
    top: 30%;
  }
`;

export const MobileImgsContainer = styled.div`
  justify-content: center;
  align-items: center;
  display: flex;
  position: relative;
  margin-left: 20%;
  height: 100%;

  @media only screen and (max-width: 820px) {
    justify-content: center;
    align-items: center;
    display: flex;
    position: relative;
    margin-left: 15%;
    height: 70%;
    margin-top: 40px;
  }
`;

export const LeftMobilePic = styled.img`
  position: relative;
  left: -12%;
  bottom: 15px;

  @media only screen and (max-width: 820px) {
    width: 50%;
    transform: translateY(40px);
    animation: unset;
  }
`;

export const RightMobilePic = styled.img`
  position: relative;
  top: -5%;

  @media only screen and (max-width: 820px) {
    width: 50%;
    transform: translateY(10px);
  }
`;
