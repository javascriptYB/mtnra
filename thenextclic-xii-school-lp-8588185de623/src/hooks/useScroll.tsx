import { useEffect, useRef, useState } from "react";
import useDevice from "./useDevice";

const useScroll = () => {
  const [data, setData] = useState<any>({
    isDown: null,
    index: 0,
  });

  const scrollRef = useRef(data);
  const device = useDevice();
  const deviceRef = useRef<{ device: { isDesktop: boolean } }>({
    device: null,
  });

  useEffect(() => {
    deviceRef["device"] = device;
  }, [device]);

  const setScrollData = (boolean, index) => {
    const data = { isDown: boolean, index };

    setData(data);
    scrollRef.current = data;
  };

  const handleMouseWheel = (event) => {
    event.preventDefault();

    if (deviceRef["device"]?.isDesktop) {
      if (event.wheelDeltaY < 0) {
        if (scrollRef.current.index < 5)
          setScrollData(true, scrollRef.current.index + 1);
      } else {
        if (scrollRef.current.index > 0) {
          if (scrollRef.current.index === 3) setScrollData(null, 0);
          else setScrollData(false, scrollRef.current.index - 1);
        }
      }
    }
  };

  let timeout;

  const debounce = (func, wait) => {
    return function executedFunction(...args) {
      clearTimeout(timeout);
      timeout = setTimeout(() => func(...args), wait);
    };
  };

  useEffect(() => {
    document.addEventListener("wheel", debounce(handleMouseWheel, 200));

    return () => document.removeEventListener("wheel", null);
  }, []);

  return data;
};

export default useScroll;
