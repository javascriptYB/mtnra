import { useEffect, useState } from "react";
import { isNullOrUndefined } from "util";

const useDevice = () => {
  const [data, setData] = useState({ isDesktop: false });

  const handleResize = (e) => {
    const newState = { isDesktop: window.innerWidth > 820 ? true : false };
    setData((oldState) => {
      if (oldState.isDesktop && !newState.isDesktop) {
        location.reload();
      }
      return newState;
    });
  };

  useEffect(() => {
    setData({ isDesktop: window?.innerWidth > 820 ? true : false });
    window.addEventListener("resize", handleResize);

    return () => window.removeEventListener("resize", null);
  }, []);

  return data;
};

export default useDevice;
