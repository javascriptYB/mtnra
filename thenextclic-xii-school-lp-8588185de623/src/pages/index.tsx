import { useContext, useEffect, useRef, useState } from "react";
import AppStore from "../components/AppStore";
import ContactUs from "../components/ContactUs";
import Discovery from "../components/Discovery";
import Features from "../components/Features";
import Intro from "../components/Intro";
import Teachers from "../components/Teachers";
import axios from "axios";
import { DataContext } from "../context";
import React from "react";

export default function Home() {
  const appStoreRef = useRef(null);
  const teachersRef = useRef(null);
  const introRef = useRef(null);
  const contactUsRef = useRef(null);

  const blocks = useContext<any>(DataContext);

  return (
    <>
      <Intro data={blocks?.bloc_home} />
      <Features data={blocks?.bloc_presentation} />
      <Discovery nextSection={appStoreRef} data={blocks?.bloc_video} />
      <AppStore
        ref={appStoreRef}
        prevSection={introRef}
        nextSection={teachersRef}
        data={blocks.bloc_app}
      />
      <Teachers
        ref={teachersRef}
        prevSection={appStoreRef}
        nextSection={contactUsRef}
        data={blocks?.bloc_temoignages}
      />
      <ContactUs ref={contactUsRef} prevSection={teachersRef} />
    </>
  );
}
