import { createGlobalStyle, ThemeProvider } from "styled-components";

import { useEffect, useState } from "react";
import axios from "axios";
import { DataContext } from "../context";

const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: satoshi;
    src: url(fonts/Satoshi-Variable.ttf);
    font-weight: 1 999;
  }
  // * {
  // backface-visibility: hidden;
  // }
  // html, body { overflow: hidden, height: 100% } 
  
  body {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    font-family: satoshi, Verdana, sans-serif;
    height: 100%;
    overflow: hidden;
    // overflow-y: scroll;
    @media only screen and (max-width: 820px) {
      overflow-y: scroll;
    }
   
  }

  body > div:first-child,
  div#__next,
  div#__next {
    height: 100%;
  }


  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  p {
    margin: 0;
    padding: 0;
  }

 .slick-track {
  display: flex;
 }
`;

const theme = {
  colors: {
    primary: "#0070f3",
  },
};

export default function App({ Component, pageProps }) {
  const [blocks, setBlocks] = useState<any>({});

  useEffect(() => {
    const Fetch = async () => {
      const data = await axios.get(
        "http://12schoolback.preprod.gear9.ma/node/5?_format=json"
      ).then(e=>setBlocks(e.data)).catch(_e=> alert("error"));

      
    };

    Fetch();
  }, []);

  return (
    <>
      <GlobalStyle />
      <ThemeProvider theme={theme}>
        <DataContext.Provider value={blocks}>
          <Component {...pageProps} />
        </DataContext.Provider>
      </ThemeProvider>
    </>
  );
}
