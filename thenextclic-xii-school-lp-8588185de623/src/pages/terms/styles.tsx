import styled from "styled-components";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  background-color: #ffffff;
  height: 100%;
  align-items: center;
  justify-content: center;
  position: relative;
  overflow: hidden;
`;

export const GeneralConditionLeftCircle = styled.div`
  height: 20vw;
  width: 20vw;
  max-width: 200px;
  max-height: 200px;
  border-radius: 100%;
  transform: scale(1.2);
  background-color: #fa6d30;
  position: absolute;
  left: -9%;
  top: 25%;

  @media only screen and (max-width: 820px) {
    display: none;
  }
`;

export const GeneralConditionRightCircle = styled.div`
  height: 20vw;
  width: 20vw;
  max-width: 350px;
  max-height: 350px;
  border-radius: 100%;
  transform: scale(2.5);
  background-color: #eff1f5;
  position: absolute;
  right: -12%;
  top: -5%;

  @media only screen and (max-width: 820px) {
    transform: scale(2);
    height: 25vw;
    width: 25vw;
    top: -2%;
    right: -5%;
  }
`;

export const GeneralConditionContent = styled.article`
  width: 60%;
  max-width: 900px;
  display: flex;
  flex-direction: column;
  z-index: 9999;
  margin: 40px 0;

  @media only screen and (max-width: 820px) {
    width: 90%;
    max-width: 600px;
  }
`;

export const GeneralConditionLogo = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 20px;
`;

export const GeneralConditionTitle = styled.h1`
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 3.5rem;
  color: #060a2b;
  margin: 40px 0;
  text-align: center;

  @media only screen and (max-width: 820px) {
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 1.8rem;
  }
`;

export const GeneralConditionArticle = styled.article`
  padding: 15px;

  & > h4 {
    padding: 8px 0;
    font-size: 1.1rem;
    font-weight: bold;
    color: #060a2b;
  }

  & > p {
    font-size: 1rem;
    font-weight: 400;
    color: #000000;
    padding-bottom: 16px;
  }

  @media only screen and (max-width: 820px) {
    padding: 0px;

    & > h4 {
      padding: 8px 0;
      font-size: 1.1rem;
      font-weight: bold;
      color: #060a2b;
    }

    & > p {
      font-size: 1rem;
      font-weight: 400;
      color: #000000;
      padding-bottom: 16px;
    }
  }
`;

export default Container;
