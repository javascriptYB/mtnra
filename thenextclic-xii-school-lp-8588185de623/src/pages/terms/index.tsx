import { useContext } from "react";
import { LogoIcon } from "../../../assets/icons";
import { createGlobalStyle, ThemeProvider } from "styled-components";
import parse from "html-react-parser";

import Footer from "../../components/Footer";
import { DataContext } from "../../context";
import Container, {
  GeneralConditionArticle,
  GeneralConditionContent,
  GeneralConditionLeftCircle,
  GeneralConditionLogo,
  GeneralConditionRightCircle,
  GeneralConditionTitle,
} from "./styles";

const GlobalStyle = createGlobalStyle`
  body {
      overflow-y: scroll !important;
  }
`;

const Terms = () => {
  const { bloc_conditions } = useContext<any>(DataContext);

  console.log({ bloc_conditions });
  return (
    <>
      <GlobalStyle />
      <Container>
        <>
          {bloc_conditions?.title ? (
            <GeneralConditionContent>
              <GeneralConditionLogo>
                <LogoIcon
                  style={{
                    backgroundColor: "white",
                    padding: "2px",
                    borderRadius: "100%",
                    width: "60px",
                    height: "60px",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    marginBottom: "1vh",
                  }}
                />
              </GeneralConditionLogo>
              <GeneralConditionTitle>
                {bloc_conditions?.title}
              </GeneralConditionTitle>

              {bloc_conditions?.conditions?.length > 0 &&
                bloc_conditions?.conditions?.map((item, index) => {
                  return (
                    <GeneralConditionArticle>
                      <h4>{item?.title}</h4>
                      {parse(item?.description || "")}
                    </GeneralConditionArticle>
                  );
                })}
            </GeneralConditionContent>
          ) : (
            <div style={{ height: "100vh" }}></div>
          )}
        </>

        <GeneralConditionLeftCircle />
        <GeneralConditionRightCircle />
        <Footer />
      </Container>
    </>
  );
};

export default Terms;
