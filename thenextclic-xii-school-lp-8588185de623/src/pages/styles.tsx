import styled from "styled-components";

const Container = styled.div`
  height: 100%;
  overflow: hidden;
  -ms-overflow-style: none; /* Internet Explorer 10+ */
  scrollbar-width: none;
  ::-webkit-scrollbar {
    display: none; /* Safari and Chrome */
  }

  @media only screen and (max-width: 820px) {
    overflow-y: scroll;
  }
`;

export const SectionScroll = styled.div`
  height: 100%;
  overflow: hidden;
  -ms-overflow-style: none; /* Internet Explorer 10+ */
  scrollbar-width: none;
  ::-webkit-scrollbar {
    display: none; /* Safari and Chrome */
  }

  @media only screen and (max-width: 820px) {
    overflow-y: scroll;
  }
`;

export default Container;
