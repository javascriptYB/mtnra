const RightArrow = ({ disabled }: { disabled: boolean }) => {
  return (
    <svg
      id="Groupe_64656"
      data-name="Groupe 64656"
      xmlns="http://www.w3.org/2000/svg"
      width="30.146"
      height="30.146"
      viewBox="0 0 30.146 30.146"
    >
      <path
        id="Tracé_16869"
        data-name="Tracé 16869"
        d="M0,0H30.146V30.146H0Z"
        fill="none"
      />
      <line
        id="Ligne_1920"
        data-name="Ligne 1920"
        x2="19"
        transform="translate(5 15)"
        fill="none"
        stroke={disabled ? "#858585" : "#fff"}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
      />
      <line
        id="Ligne_1921"
        data-name="Ligne 1921"
        y1="4"
        x2="4"
        transform="translate(20 15)"
        fill="none"
        stroke={disabled ? "#858585" : "#fff"}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
      />
      <line
        id="Ligne_1922"
        data-name="Ligne 1922"
        x2="4"
        y2="4"
        transform="translate(20 11)"
        fill="none"
        stroke={disabled ? "#858585" : "#fff"}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
      />
    </svg>
  );
};

export default RightArrow;
