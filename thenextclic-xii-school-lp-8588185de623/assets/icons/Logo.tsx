const Logo = ({ style }: { style: object }) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      width="56"
      height="56"
      viewBox="0 0 56 56"
      style={style}
    >
      <defs>
        <linearGradient
          id="linear-gradient"
          x1="0.197"
          y1="0.153"
          x2="0.772"
          y2="0.909"
          gradientUnits="objectBoundingBox"
        >
          <stop offset="0" stopColor="#ffbd44" />
          <stop offset="0.222" stopColor="#fb5c42" />
          <stop offset="0.484" stopColor="#e61474" />
          <stop offset="0.749" stopColor="#a8147b" />
          <stop offset="1" stopColor="#170330" />
        </linearGradient>
      </defs>
      <g
        id="Groupe_63275"
        data-name="Groupe 63275"
        transform="translate(-164.857 -1539.857)"
      >
        <g
          id="Groupe_63274"
          data-name="Groupe 63274"
          transform="translate(164.857 1539.857)"
        >
          <path
            id="Soustraction_1"
            data-name="Soustraction 1"
            d="M28,56a27.883,27.883,0,0,1-15.278-4.542c-.45-.413-.9-.822-1.363-1.241l-.062-.057c-.417-.378-.847-.769-1.269-1.156L20.585,32.3l2.6,3.978,6.284,9.15V36.391l-5.787-8.834,5.787-8.594,25.036.008a27.944,27.944,0,0,1-3.289,24.684A28.088,28.088,0,0,1,38.9,53.8,27.837,27.837,0,0,1,28,56ZM41.874,22.427V45.433h5.479V22.427Zm-8.937,0V45.433h5.479V22.427ZM6.1,45.425h0A27.962,27.962,0,0,1,8.2,8.2a28.05,28.05,0,0,1,8.9-6A27.851,27.851,0,0,1,28,0a25.9,25.9,0,0,1,6.262.764,39.765,39.765,0,0,1,6.057,2.087A28.179,28.179,0,0,1,51.971,13.509H26.885l-6.3,9.307-6.093-9.307L11.324,18.15l6.151,9.4L6.1,45.425Z"
            transform="translate(0 0)"
            fill="url(#linear-gradient)"
          />
        </g>
      </g>
    </svg>
  );
};

export default Logo;
