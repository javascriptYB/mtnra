const Coaching = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="29"
      height="32"
      viewBox="0 0 29 32"
    >
      <g id="noun-mentor-2593649" transform="translate(-97.585 -4.48)">
        <path
          id="Tracé_16880"
          data-name="Tracé 16880"
          d="M125.7,11.757c-1.915-4.288-6.932-7.277-12.148-7.277a10.9,10.9,0,0,0-9.474,5.036,15.6,15.6,0,0,0-2.41,6.985c-.132,1.949-2.905,5.88-3.961,7.245a.6.6,0,0,0-.066.617.589.589,0,0,0,.528.325h2.311v4.028a3.8,3.8,0,0,0,3.829,3.769h4.522V35.9a.594.594,0,0,0,1.188,0V32.452h2.707a.585.585,0,1,0,0-1.169h-8.352a2.628,2.628,0,0,1-2.641-2.6V24.07a.587.587,0,0,0-.594-.585H99.425c1.156-1.624,3.367-4.906,3.5-6.952,0-.1.924-10.883,10.663-10.883,4.754,0,9.276,2.7,11.026,6.595,1.849,4.191.4,9.194-4.159,14.067a.6.6,0,0,0-.165.39v9.161a.594.594,0,0,0,1.188,0v-8.9c6.437-7.05,5.414-12.54,4.225-15.2Z"
          fill="#fff"
        />
        <path
          id="Tracé_16881"
          data-name="Tracé 16881"
          d="M270.892,82.339a7.57,7.57,0,1,1,7.659-7.57A7.639,7.639,0,0,1,270.892,82.339Zm0-14.2a6.628,6.628,0,1,0,6.734,6.627A6.689,6.689,0,0,0,270.892,68.142Z"
          transform="translate(-155.852 -59.081)"
          fill="#fff"
          stroke="#fff"
          strokeWidth="0.5"
        />
        <path
          id="Tracé_16882"
          data-name="Tracé 16882"
          d="M320.933,141.974l-.231-2.144a.46.46,0,0,0-.462-.39l-2.278.228a.4.4,0,0,0-.3.162.542.542,0,0,0-.1.325.386.386,0,0,0,.165.292.563.563,0,0,0,.33.1l1.32-.13-2.476,2.956-1.651-1.592a.508.508,0,0,0-.33-.13.422.422,0,0,0-.3.13L312.02,144.7a.542.542,0,0,0-.1.325.481.481,0,0,0,.132.292.407.407,0,0,0,.3.1h.033a.422.422,0,0,0,.3-.13l2.311-2.6,1.684,1.624a.427.427,0,0,0,.33.13.348.348,0,0,0,.3-.162l2.674-3.216.1,1.04a.34.34,0,0,0,.165.292.563.563,0,0,0,.33.1.4.4,0,0,0,.3-.162.469.469,0,0,0,.066-.357Z"
          transform="translate(-201.701 -127.131)"
          fill="#fff"
          stroke="#fff"
          strokeWidth="0.3"
        />
      </g>
    </svg>
  );
};

export default Coaching;
