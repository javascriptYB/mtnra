const Facebook = ({ style }: { style: object }) => {
  return (
    <svg
      style={style}
      id="RS"
      xmlns="http://www.w3.org/2000/svg"
      width="36"
      height="36"
      viewBox="0 0 36 36"
    >
      <g
        id="Ellipse_2724"
        data-name="Ellipse 2724"
        fill="#fff"
        stroke="#d8e8ff"
        strokeWidth="1"
      >
        <circle cx="18" cy="18" r="18" stroke="none" />
        <circle cx="18" cy="18" r="17.5" fill="none" />
      </g>
      <g id="Groupe_63276" data-name="Groupe 63276" transform="translate(9 9)">
        <path
          id="Tracé_15494"
          data-name="Tracé 15494"
          d="M0,0H18V18H0Z"
          fill="none"
        />
        <path
          id="Tracé_15495"
          data-name="Tracé 15495"
          d="M12.462,10.555h1.951l.78-2.976H12.462V6.092c0-.766,0-1.488,1.56-1.488h1.17V2.1c-.254-.032-1.215-.1-2.229-.1A3.307,3.307,0,0,0,9.341,5.5V7.58H7v2.976H9.341v6.324h3.121Z"
          transform="translate(-1.915 -0.44)"
          fill="#bf2172"
        />
      </g>
    </svg>
  );
};

export default Facebook;
