const BackgroundLogo = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      width="736.953"
      height="434.405"
      viewBox="0 0 736.953 434.405"
    >
      <defs>
        <radialGradient
          id="radial-gradient"
          cx="0.254"
          cy="0.227"
          r="0.744"
          gradientUnits="objectBoundingBox"
        >
          <stop offset="0" stopColor="#e5e7fb" />
          <stop offset="0.46" stopColor="#e5e7fb" stopOpacity="0.451" />
          <stop offset="1" stopColor="#e5e7fb" stopOpacity="0" />
        </radialGradient>
      </defs>
      <g
        id="Groupe_64647"
        data-name="Groupe 64647"
        transform="translate(62.629)"
      >
        <path
          id="Union_16"
          data-name="Union 16"
          d="M0,403.5,190.24,142.452,123.374,46.833,158.644,0l65.677,95.686L294.006.066V0H736.953V55.273H325.807l-66.548,91.316L362.39,296.842l.216,92.094L225.514,192.894,49.509,434.4Z"
          transform="translate(-62.629 0)"
          fill="url(#radial-gradient)"
        />
        <rect
          id="Rectangle_11179"
          data-name="Rectangle 11179"
          width="66"
          height="297"
          transform="translate(372.324 94.405)"
          fill="url(#radial-gradient)"
        />
        <rect
          id="Rectangle_11180"
          data-name="Rectangle 11180"
          width="66"
          height="297"
          transform="translate(469.324 94.405)"
          fill="url(#radial-gradient)"
        />
      </g>
    </svg>
  );
};

export default BackgroundLogo;
