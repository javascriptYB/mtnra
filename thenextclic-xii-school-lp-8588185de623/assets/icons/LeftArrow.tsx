const LeftArrow = ({ disabled }: { disabled: boolean }) => {
  return (
    <svg
      id="Groupe_64656"
      data-name="Groupe 64656"
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
    >
      <path
        id="Tracé_16869"
        data-name="Tracé 16869"
        d="M24,0H0V24H24Z"
        fill="none"
      />
      <line
        id="Ligne_1920"
        data-name="Ligne 1920"
        x1="14"
        transform="translate(5 12)"
        fill="none"
        stroke={disabled ? "#858585" : "#fff"}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
      />
      <line
        id="Ligne_1921"
        data-name="Ligne 1921"
        x1="4"
        y1="4"
        transform="translate(5 12)"
        fill="none"
        stroke={disabled ? "#858585" : "#fff"}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
      />
      <line
        id="Ligne_1922"
        data-name="Ligne 1922"
        x1="4"
        y2="4"
        transform="translate(5 8)"
        fill="none"
        stroke={disabled ? "#858585" : "#fff"}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
      />
    </svg>
  );
};

export default LeftArrow;
