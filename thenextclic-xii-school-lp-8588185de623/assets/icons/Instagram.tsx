const Instagram = () => {
  return (
    <svg
      id="RS"
      xmlns="http://www.w3.org/2000/svg"
      width="36"
      height="36"
      viewBox="0 0 36 36"
    >
      <g
        id="Ellipse_2724"
        data-name="Ellipse 2724"
        fill="#fff"
        stroke="#d8e8ff"
        strokeWidth="1"
      >
        <circle cx="18" cy="18" r="18" stroke="none" />
        <circle cx="18" cy="18" r="17.5" fill="none" />
      </g>
      <g id="Groupe_64665" data-name="Groupe 64665" transform="translate(9 9)">
        <path
          id="Tracé_15496"
          data-name="Tracé 15496"
          d="M0,0H18V18H0Z"
          fill="none"
        />
        <path
          id="Tracé_15497"
          data-name="Tracé 15497"
          d="M9.5,7.25A2.25,2.25,0,1,0,11.75,9.5,2.25,2.25,0,0,0,9.5,7.25Zm0-1.5A3.75,3.75,0,1,1,5.75,9.5,3.75,3.75,0,0,1,9.5,5.75Zm4.875-.187a.938.938,0,1,1-.937-.937A.938.938,0,0,1,14.375,5.563ZM9.5,3.5c-1.856,0-2.159.005-3.022.043a4.074,4.074,0,0,0-1.348.249A2.34,2.34,0,0,0,3.792,5.13a4.069,4.069,0,0,0-.248,1.348C3.5,7.306,3.5,7.6,3.5,9.5c0,1.856.005,2.159.043,3.022a4.091,4.091,0,0,0,.248,1.348,2.332,2.332,0,0,0,1.336,1.337,4.083,4.083,0,0,0,1.35.25C7.306,15.5,7.6,15.5,9.5,15.5c1.856,0,2.159-.005,3.022-.044a4.1,4.1,0,0,0,1.348-.248,2.191,2.191,0,0,0,.81-.527,2.168,2.168,0,0,0,.528-.81,4.1,4.1,0,0,0,.249-1.35c.039-.828.044-1.118.044-3.022,0-1.856-.005-2.159-.044-3.022a4.082,4.082,0,0,0-.249-1.349,2.183,2.183,0,0,0-.527-.81,2.163,2.163,0,0,0-.81-.528,4.073,4.073,0,0,0-1.349-.248C11.694,3.5,11.4,3.5,9.5,3.5ZM9.5,2c2.038,0,2.292.007,3.091.045a5.525,5.525,0,0,1,1.821.349,3.657,3.657,0,0,1,1.329.865,3.681,3.681,0,0,1,.865,1.329,5.54,5.54,0,0,1,.349,1.821c.035.8.045,1.054.045,3.092s-.007,2.292-.045,3.091a5.545,5.545,0,0,1-.349,1.821,3.829,3.829,0,0,1-2.194,2.194,5.54,5.54,0,0,1-1.821.349c-.8.035-1.054.045-3.091.045s-2.292-.007-3.092-.045a5.545,5.545,0,0,1-1.821-.349,3.829,3.829,0,0,1-2.194-2.194,5.521,5.521,0,0,1-.349-1.821C2.01,11.792,2,11.538,2,9.5s.007-2.292.045-3.092a5.521,5.521,0,0,1,.349-1.821A3.829,3.829,0,0,1,4.587,2.394a5.521,5.521,0,0,1,1.821-.349C7.208,2.01,7.462,2,9.5,2Z"
          transform="translate(-0.5 -0.5)"
          fill="#bf2172"
        />
      </g>
    </svg>
  );
};

export default Instagram;
