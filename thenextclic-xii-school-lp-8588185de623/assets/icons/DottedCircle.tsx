const DottedCircle = () => {
  return (
    <svg
      id="Composant_48_11"
      data-name="Composant 48 – 11"
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      width="623.15"
      height="623.149"
      viewBox="0 0 623.15 623.149"
    >
      <defs>
        <linearGradient
          id="linear-gradient"
          x1="1"
          y1="0.5"
          x2="0"
          y2="0.719"
          gradientUnits="objectBoundingBox"
        >
          <stop offset="0" stopColor="#e4067e" />
          <stop offset="1" stopColor="#fa6d30" />
        </linearGradient>
      </defs>
      <g
        id="Groupe_64654"
        data-name="Groupe 64654"
        transform="translate(0 623.149) rotate(-90)"
      >
        <g
          id="Rectangle_11183"
          data-name="Rectangle 11183"
          transform="translate(604 19.075) rotate(90)"
          fill="none"
          stroke="#f2c1d6"
          strokeWidth="2"
          strokeDasharray="9"
        >
          <rect width="585" height="585" rx="292.5" stroke="none" />
          <rect x="1" y="1" width="583" height="583" rx="291.5" fill="none" />
        </g>
        <g id="active" transform="translate(-737.117 -6543.171) rotate(-45)">
          <circle
            id="Ellipse_2981"
            data-name="Ellipse 2981"
            cx="19"
            cy="19"
            r="19"
            transform="translate(-4124 5277)"
            fill="#edeaf1"
          />
          <circle
            id="Ellipse_2979"
            data-name="Ellipse 2979"
            cx="13"
            cy="13"
            r="13"
            transform="translate(-4118 5283)"
            fill="#bf2172"
            opacity="0.05"
          />
          <circle
            id="Ellipse_2975"
            data-name="Ellipse 2975"
            cx="8"
            cy="8"
            r="8"
            transform="translate(-4113 5288)"
            fill="url(#linear-gradient)"
          />
        </g>
        <g
          id="active-2"
          data-name="active"
          transform="translate(4417.075 -5277)"
        >
          <circle
            id="Ellipse_2981-2"
            data-name="Ellipse 2981"
            cx="19"
            cy="19"
            r="19"
            transform="translate(-4124 5277)"
            fill="#edeaf1"
          />
          <circle
            id="Ellipse_2979-2"
            data-name="Ellipse 2979"
            cx="13"
            cy="13"
            r="13"
            transform="translate(-4118 5283)"
            fill="#bf2172"
            opacity="0.05"
          />
          <circle
            id="Ellipse_2975-2"
            data-name="Ellipse 2975"
            cx="8"
            cy="8"
            r="8"
            transform="translate(-4113 5288)"
            fill="url(#linear-gradient)"
          />
        </g>
        <g
          id="active-3"
          data-name="active"
          transform="translate(-6543.17 1360.266) rotate(-135)"
        >
          <circle
            id="Ellipse_2981-3"
            data-name="Ellipse 2981"
            cx="19"
            cy="19"
            r="19"
            transform="translate(-4124 5277)"
            fill="#edeaf1"
          />
          <circle
            id="Ellipse_2979-3"
            data-name="Ellipse 2979"
            cx="13"
            cy="13"
            r="13"
            transform="translate(-4118 5283)"
            fill="#bf2172"
            opacity="0.05"
          />
          <circle
            id="Ellipse_2975-3"
            data-name="Ellipse 2975"
            cx="8"
            cy="8"
            r="8"
            transform="translate(-4113 5288)"
            fill="url(#linear-gradient)"
          />
        </g>
        <g
          id="active-4"
          data-name="active"
          transform="translate(1360.266 7166.32) rotate(135)"
        >
          <circle
            id="Ellipse_2981-4"
            data-name="Ellipse 2981"
            cx="19"
            cy="19"
            r="19"
            transform="translate(-4124 5277)"
            fill="#edeaf1"
          />
          <circle
            id="Ellipse_2979-4"
            data-name="Ellipse 2979"
            cx="13"
            cy="13"
            r="13"
            transform="translate(-4118 5283)"
            fill="#bf2172"
            opacity="0.05"
          />
          <circle
            id="Ellipse_2975-4"
            data-name="Ellipse 2975"
            cx="8"
            cy="8"
            r="8"
            transform="translate(-4113 5288)"
            fill="url(#linear-gradient)"
          />
        </g>
        <g
          id="active-5"
          data-name="active"
          transform="translate(7166.319 -737.117) rotate(45)"
        >
          <circle
            id="Ellipse_2981-5"
            data-name="Ellipse 2981"
            cx="19"
            cy="19"
            r="19"
            transform="translate(-4124 5277)"
            fill="#edeaf1"
          />
          <circle
            id="Ellipse_2979-5"
            data-name="Ellipse 2979"
            cx="13"
            cy="13"
            r="13"
            transform="translate(-4118 5283)"
            fill="#bf2172"
            opacity="0.05"
          />
          <circle
            id="Ellipse_2975-5"
            data-name="Ellipse 2975"
            cx="8"
            cy="8"
            r="8"
            transform="translate(-4113 5288)"
            fill="url(#linear-gradient)"
          />
        </g>
        <g
          id="active-6"
          data-name="active"
          transform="translate(-5277 -3793.926) rotate(-90)"
        >
          <circle
            id="Ellipse_2981-6"
            data-name="Ellipse 2981"
            cx="19"
            cy="19"
            r="19"
            transform="translate(-4124 5277)"
            fill="#edeaf1"
          />
          <circle
            id="Ellipse_2979-6"
            data-name="Ellipse 2979"
            cx="13"
            cy="13"
            r="13"
            transform="translate(-4118 5283)"
            fill="#bf2172"
            opacity="0.05"
          />
          <circle
            id="Ellipse_2975-6"
            data-name="Ellipse 2975"
            cx="8"
            cy="8"
            r="8"
            transform="translate(-4113 5288)"
            fill="url(#linear-gradient)"
          />
        </g>
        <g
          id="active-7"
          data-name="active"
          transform="translate(-3793.925 5900.149) rotate(180)"
        >
          <circle
            id="Ellipse_2981-7"
            data-name="Ellipse 2981"
            cx="19"
            cy="19"
            r="19"
            transform="translate(-4124 5277)"
            fill="#edeaf1"
          />
          <circle
            id="Ellipse_2979-7"
            data-name="Ellipse 2979"
            cx="13"
            cy="13"
            r="13"
            transform="translate(-4118 5283)"
            fill="#bf2172"
            opacity="0.05"
          />
          <circle
            id="Ellipse_2975-7"
            data-name="Ellipse 2975"
            cx="8"
            cy="8"
            r="8"
            transform="translate(-4113 5288)"
            fill="url(#linear-gradient)"
          />
        </g>
        <g
          id="active-8"
          data-name="active"
          transform="translate(5900.148 4417.075) rotate(90)"
        >
          <circle
            id="Ellipse_2981-8"
            data-name="Ellipse 2981"
            cx="19"
            cy="19"
            r="19"
            transform="translate(-4124 5277)"
            fill="#edeaf1"
          />
          <circle
            id="Ellipse_2979-8"
            data-name="Ellipse 2979"
            cx="13"
            cy="13"
            r="13"
            transform="translate(-4118 5283)"
            fill="#bf2172"
            opacity="0.05"
          />
          <circle
            id="Ellipse_2975-8"
            data-name="Ellipse 2975"
            cx="8"
            cy="8"
            r="8"
            transform="translate(-4113 5288)"
            fill="url(#linear-gradient)"
          />
        </g>
      </g>
    </svg>
  );
};

export default DottedCircle;
