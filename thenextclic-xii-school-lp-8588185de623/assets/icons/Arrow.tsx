const Arrow = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="32"
      height="32"
      viewBox="0 0 32 32"
    >
      <g id="noun-goal-117084" transform="translate(-92.398 -22.399)">
        <path
          id="Tracé_16866"
          data-name="Tracé 16866"
          d="M123.7,26.34h-3.247V23.094a.7.7,0,0,0-1.187-.492l-3,3a15.582,15.582,0,1,0,4.925,4.925l3-3a.7.7,0,0,0-.492-1.188Zm-1.523,12.473a14.195,14.195,0,1,1-6.928-12.194l-2.014,2.014a.7.7,0,0,0-.2.492v1.5a9.635,9.635,0,0,0-5.049-1.439h0a9.65,9.65,0,1,0,8.189,4.579h1.5a.7.7,0,0,0,.492-.2l2.015-2.014a14.123,14.123,0,0,1,2,7.269Zm-9.146-6.034-2.992,2.992a3.677,3.677,0,1,0,.984.984l2.993-2.993h.464a8.232,8.232,0,1,1-6.5-3.188h0a8.239,8.239,0,0,1,5.049,1.738v.468Zm-2.767,6.034a2.289,2.289,0,1,1-1.239-2.028l-1.536,1.536a.7.7,0,1,0,.984.984l1.536-1.536a2.264,2.264,0,0,1,.255,1.044Zm9.543-8.869-.018.018-2.409,2.408h-2.958V29.412l2.4-2.4.027-.027,2.207-2.207v2.261a.7.7,0,0,0,.7.7h2.263Z"
          transform="translate(0)"
          fill="#fff"
        />
      </g>
    </svg>
  );
};

export default Arrow;
