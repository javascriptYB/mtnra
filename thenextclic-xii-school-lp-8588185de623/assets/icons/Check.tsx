const Check = (props: any) => {
  return (
    <svg
      id="Groupe_64877"
      data-name="Groupe 64877"
      xmlns="http://www.w3.org/2000/svg"
      width="18"
      height="18"
      viewBox="0 0 18 18"
      {...props}
    >
      <path
        id="Tracé_17084"
        data-name="Tracé 17084"
        d="M0,0H18V18H0Z"
        fill="none"
      />
      <path
        id="Tracé_17085"
        data-name="Tracé 17085"
        d="M5,10.75,8.75,14.5,16.25,7"
        transform="translate(-1.25 -1.75)"
        fill="none"
        stroke="#1dac04"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
      />
    </svg>
  );
};

export default Check;
