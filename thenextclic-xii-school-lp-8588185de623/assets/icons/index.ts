export {default as LogoIcon } from "./Logo";
export {default as FacebookIcon } from "./Facebook";
export {default as BackgroundLogoIcon } from "./BackgroundLogo";
export {default as DottedCircleIcon } from "./DottedCircle";
export {default as LeftArrowIcon } from "./LeftArrow";
export {default as CheckIcon } from "./Check";
export {default as InstagramIcon } from "./Instagram";
export {default as ArrowIcon} from "./Arrow";
export {default as ChillIcon} from "./Chill";
export {default as HomeworkIcon} from "./Homework";
export {default as SeminarIcon} from "./Seminar";
export {default as CoachingIcon} from "./Coaching";
export {default as IndividualIcon} from "./Individual";
export {default as GroupIcon} from "./Group";
export {default as RightArrowIcon} from "./RightArrow";
